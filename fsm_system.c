/**
\file
\brief Overall system control state machine

\author S.Parker
\par History:
\li	15/06/15 SP - initial creation
*/

// compiler standard include files
#include <stdint.h>

// processor standard include files
#include "F2806x_Device.h"
#include "F2806x_GlobalPrototypes.h"

// local include files
#include "fsm.h"

/*  =========================================================================
__Definitions()
============================================================================ */

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Local_Function_Prototypes()
============================================================================ */

/** #name State Functions */
//@{
void system_state_init(void);
void system_state_hold(void);
void system_state_dev(void);
//@}

/* =========================================================================
__Variables()
============================================================================ */

/// Open loop commands from background/fsm
type_background background =
	{
		SYS_MODE_NONE,
		{	// hbridge
			0, // init
			HBRIDGE_MODE_NONE, // mode
			0, // enable
			0, // mod_depth
			0 // voltage
		},
		{
			0, // init
			BOOST_MODE_NONE, // mode
			0, // enable
			0, // dutycycle
			0, // current
			0 // voltage
		}
	};

/// State machine pointer and variables
type_state system_state = { &system_state_init, (statePtr)0, 1, 0 };

/* =========================================================================
__Exported_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Runs the current hbridge state function

\author S.Parker
\par History:
\li	15/06/15 SP - initial creation
*/
void run_system_fsm(void)
{
	// Flag state transition
	system_state.first_time = (system_state.prev != system_state.current);
	system_state.prev = system_state.current;

	// Call current state
	CALL_STATE(system_state);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns the name of the current state

\author S.Parker
\par History:
\li	17/06/15 SP - initial creation
*/
char* get_system_state_name(void)
{
	return system_state.state_name;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns the current operating mode

\author S.Parker
\par History:
\li	17/06/15 SP - initial creation
*/
Uint16 get_system_mode(void)
{
	return background.system_mode;
}

/* =========================================================================
__Local_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialisation State

\author S.Parker
\par History:
\li	15/06/15 SP - initial creation
*/
void system_state_init(void)
{
	if(system_state.first_time == 1)
	{
		system_state.state_name = "Init  ";
		system_state.first_time = 0;
	}

	/* State transitions */
	// Wait for boost and hbridge initialisation
	if( (background.hbridge.init == 1)&&(background.boost.init == 1) )
	{
		system_state.current = (statePtr)&system_state_hold;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Waits for flag to start

\author S.Parker
\par History:
\li	15/06/15 SP - initial creation
*/
void system_state_hold(void)
{
	if(system_state.first_time == 1)
	{
		system_state.state_name = "Hold";
		system_state.first_time = 0;

		disable_boost();
		disable_hbridge();
	}

	/* State transitions */
	// Development mode
	if(background.system_mode==SYS_MODE_DEV)
	{
		if(is_hbridge_hold()&&is_boost_hold())
		{
			system_state.current = (statePtr)&system_state_dev;
			return;
		}
		else
		{
			background.system_mode = SYS_MODE_NONE;
			return;
		}
	}
	// Production mode
	else if(background.system_mode==SYS_MODE_PROD)
	{
		if(is_hbridge_hold()&&is_boost_hold())
		{
			//system_state.current = (statePtr)&system_state_prod;
			return;
		}
		else
		{
			background.system_mode = SYS_MODE_NONE;
			return;
		}
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Allows development operation
- the keyboard is used to enable and disable different features.
- system state machine has no knowledge of faults/recovery.

\author S.Parker
\par History:
\li	15/06/15 SP - initial creation
*/
void system_state_dev(void)
{
	if(system_state.first_time == 1)
	{
		system_state.state_name = "Dev ";
		system_state.first_time = 0;
		
		background.system_mode = SYS_MODE_DEV;
		
		// Initially zero out commands
		set_hbridge_moddepth(0);
		set_boost_dutycycle(100);
	}

	/* State transitions */
	if(background.system_mode==SYS_MODE_NONE)
	{
		system_state.current = (statePtr)&system_state_hold;
	}

}

// end fsm_system.c
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
