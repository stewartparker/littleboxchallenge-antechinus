/**
\file
\brief Initialise SCI-A for COM port operation

Provide functions to interpret strings/numbers into bytes
Provide round robbin FIFO buffer for terminal

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
\li 20/10/14 LB - added further types
*/

// compiler standard include files
#include <stdint.h>
#include <math.h>
#include <float.h>

// processor standard include files
#include "F2806x_Device.h"
#include "F2806x_GlobalPrototypes.h"

// local include files
#include "serial_com.h"

/*  =========================================================================
__Definitions()
============================================================================ */

/** @name Output settings */
//@{
/// Correctly round floating point
#define USE_ROUNDING 0
/// Check for NaN / inf
#define INPUT_CONDITIONING 0
//@}

/** @name Fifo specifications */
//@{
/// TX fifo size
#define TX_FIFO_LENGTH 128
/// Mask for fifo wrap
#define TX_FIFO_MASK 0x7F // 0x7F for 128 byte long tx fifo
//@}

/** @name String lenghs for different variables */
//@{
#define _16_BIT_STRING_MAX 6  //-32768
#define _32_BIT_STRING_MAX 11 //
#define _64_BIT_STRING_MAX 20 //18 446 774 073 709 551 615
//@}

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Variables()
============================================================================ */

const Uint8 hex_decode[16] = {"0123456789ABCDEF"};

// FIFO buffer for data waiting to be output to SCI
volatile Uint8
	com_tx_fifo[TX_FIFO_LENGTH];
// Start and end of FIFO data
volatile Uint16
	com_tx_fifo_start = 0, // Points to the first valid byte
	com_tx_fifo_length = 0; // Number of valid chars

/* =========================================================================
__Local_Function_Prototypes()
============================================================================ */

/// Put fractional part of float onto display
void putfract_com(uint32_t fract, uint16_t dig);

/// Put value into fifo
inline void com_tx_fifo_push(uint16_t in);

/* =========================================================================
__Local_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Add byte to serial com FIFO - truncate if full

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
*/
inline void com_tx_fifo_push(uint16_t in)
{
	// check if fifo is full
	if(com_tx_fifo_length < TX_FIFO_LENGTH)
	{
		com_tx_fifo[(com_tx_fifo_start+com_tx_fifo_length)&TX_FIFO_MASK] = in;
		com_tx_fifo_length++;
	}
//	else
//	{
//		return 0; // failed to add to fifo
//	}
//	return 1; // successfully added to fifo
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Convert fractional part of decimal to string, used by floating point display
functions.
 
\author L.Baker
\par History:
\li	24/10/14 LB - initial creation
*/
void putfract_com(uint32_t fract, uint16_t dig)
{
	char str[_32_BIT_STRING_MAX+1]={'\0'};
	int n=dig;
    while (n-- > 0)
    {
        str[n]=(fract % 10) + '0';
        fract /= 10;
    }
	puts_com(str);
}

/* =========================================================================
__Exported_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Reset the FIFO to empty

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
*/
void com_tx_fifo_reset(void)
{
	com_tx_fifo_start = 0;
	com_tx_fifo_length = 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Write up to 4 values in serial com FIFO to SCI-A transmit buffer

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
*/
void com_tx_fifo_run(void)
{
	Uint8 to_add;
//	// Check if anything is in com_tx_fifo
//	if(!com_tx_fifo_length)
//	{
//		return 0; // nothing in com_tx_fifo to write
//	}
//
//	// Check for room in SCI FIFO registers
//	if((4 - SciaRegs.SCIFFTX.bit.TXFFST) == 0)
//	{
//		return 0; // no room in SCI FIFO to write
//	}

	to_add = (4 - SciaRegs.SCIFFTX.bit.TXFFST);

	// Write to SCI FIFO registers
	if(com_tx_fifo_length)
	{
		while(to_add)
		{
			to_add--;
			SciaRegs.SCITXBUF = com_tx_fifo[com_tx_fifo_start];
			com_tx_fifo_start = (com_tx_fifo_start+1)&TX_FIFO_MASK;
			com_tx_fifo_length--;
	
			if(!com_tx_fifo_length)
				break;
		}
	}
//	else
//	{
//		return 0;
//	}
//	return 1;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Set up SCI-A for COM port operation

User defined baud rate not implemented because I'm lazy

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
*/
void InitSciA(void)
{
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.SCIAENCLK = 1;

	GpioCtrlRegs.GPAMUX2.bit.GPIO28 = 1; // as Rx
	GpioCtrlRegs.GPAMUX2.bit.GPIO29 = 1; // as Tx
	EDIS;

	SciaRegs.SCICCR.all =0x0007;   // 1 stop bit,  No loopback, No parity,
									//	8 char bits, async mode, idle-line protocol
	SciaRegs.SCICTL1.all =0x0003;  // enable TX, RX, internal SCICLK,
	SciaRegs.SCICTL2.all =0x0003;  // Disable RX ERR, SLEEP, TXWAKE
	SciaRegs.SCICTL2.bit.TXINTENA =1;
	SciaRegs.SCICTL2.bit.RXBKINTENA =1;

	SciaRegs.SCIHBAUD    =0;
	SciaRegs.SCILBAUD    =48; // 57600 @ 25MHz LSPCLK
	SciaRegs.SCICTL1.all =0x0023;     // Relinquish SCI from Reset

	SciaRegs.SCIFFTX.all=0xE040;
	SciaRegs.SCIFFRX.all=0x2044;
	SciaRegs.SCIFFCT.all=0x0;

	SciaRegs.SCIPRI.bit.FREE = 1; // dont stop on emulator suspend

	com_tx_fifo_start = 0;
	com_tx_fifo_length = 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Directly place byte into SCI-A transmit buffer

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
*/
void scia_xmit(uint16_t a)
{
    SciaRegs.SCITXBUF = a&0x00FF;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Put a character into the FIFO.

\author L.Baker
\par History:
\li	16/10/14 LB - initial creation
*/
void putc_com(uint16_t c)
{
//	if(!com_tx_fifo_push(c))
//	{
//		com_tx_fifo_run();
		com_tx_fifo_push(c&0x00FF);
//	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Put a string into the FIFO - string longer than fifo will be truncated

\author L.Baker
\par History:
\li	16/10/14 LB - initial creation
*/
void puts_com(char* str)
{
	Uint16 i;
	for(i=0; (str[i]!='\0')&&(i < TX_FIFO_LENGTH); i++ )
	{
		com_tx_fifo_push(str[i]);
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Convert int16 to string

\author L.Baker
\par History:
\li	16/10/14 LB - initial creation
*/
void putd_com(int16_t bin)
{
	char str[_16_BIT_STRING_MAX +1]={'\0'};
	Uint16 mag;
	int n=_16_BIT_STRING_MAX;
	
	mag = abs(bin);
    while (n-- > 0)
    {
        str[n]=((mag % 10) + '0');
        mag /= 10;
        if(mag==0)
        	break;
    }
	if(bin < 0)
	{
		n--;
		str[n]='-';
	}
	puts_com(&str[n]);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Convert Uint16 to string

\author L.Baker
\par History:
\li	16/10/14 LB - initial creation
*/
void putud_com(uint16_t bin)
{
	char str[_16_BIT_STRING_MAX +1]={'\0'};
	int n=_16_BIT_STRING_MAX;
    while (n-- > 0)
    {
        str[n] =((bin % 10) + '0');
        bin /= 10;
        if(bin==0)
        	break;
    }

	puts_com(&str[n]);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Convert int32 to string

\author L.Baker
\par History:
\li	16/10/14 LB - initial creation
*/
void putl_com(int32_t bin)
{
	char str[_32_BIT_STRING_MAX+1]={'\0'};
	Uint32 mag;
	int n=_32_BIT_STRING_MAX;
	
	mag = labs(bin);
    while (n-- > 0)
    {
        str[n]=((mag % 10) + '0');
        mag /= 10;
        if(mag==0)
        	break;
    }
	if(bin < 0)
	{
		n--;
		str[n]='-';
	}
	puts_com(&str[n]);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Convert Uint32 to string

\author L.Baker
\par History:
\li	16/10/14 LB - initial creation
*/
void putul_com(uint32_t bin)
{
	char str[_32_BIT_STRING_MAX+1]={'\0'};
	int n=_32_BIT_STRING_MAX;
    while (n-- > 0)
    {
        str[n]=(bin % 10) + '0';
        bin /= 10;
        if(bin==0)
        	break;
    }
	puts_com(&str[n]);
}

void putull_com(uint64_t bin)
{
	char str[_64_BIT_STRING_MAX+1]={'\0'};
	int n=_64_BIT_STRING_MAX;
    while (n-- > 0)
    {
        str[n]=(bin % 10) + '0';
        bin /= 10;
        if(bin==0)
        	break;
    }
	puts_com(&str[n]);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Naive hexidecimal display implementation
Writes out 16 bit value in the form 0xXX

\author L.Baker
\par History:
\li	20/10/14 LB - initial creation
*/
void putxx_com(uint16_t word)
{
	Int8 i = 16;
	com_tx_fifo_push('0');
	com_tx_fifo_push('x');
	while(i>0)
	{
		i-=4;
		com_tx_fifo_push(hex_decode[0x0f&(word >>i)]);
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Naive hexidecimal display implementation
Writes out 32 bit value in the form 0xXXXXXXXX

\author L.Baker
\par History:
\li	20/10/14 LB - initial creation
*/
void putxxxx_com(uint32_t dword)
{
	Int8 i = 32;
	com_tx_fifo_push('0');
	com_tx_fifo_push('x');
	while(i>0)
	{
		i-=4;
		com_tx_fifo_push(hex_decode[0x0f&(dword >>i)]);
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Convert 32-bit float into scientific notation

Rounding and input conditioning determined by USE_ROUNDING and
INPUT_CONDITIONING definitions.

\author L.Baker
\par History:
\li	24/10/14 LB - initial creation
*/
void putsci_com(float32 real,uint16_t dig)
{
	int16 exp=0;
	Uint16 num=0;
	Uint32 fract;

	if(real < 0)
	{
		com_tx_fifo_push('-');
	}
	real =fabs(real);

#ifdef INPUT_CONDITIONING
	// check for NaN and overflow
	if(isnan(real))
	{
		puts_com((char*)"NaN");
		return;
	}
	if(isinf(real))
	{
		puts_com((char*)"INF");
		return;
	}
	// limit precision
	if(dig > FLT_DIG) dig=FLT_DIG;
#endif

	// convert to base 10 by recursive division
	if(real >= 10.0)
	{
		while(real >=10.0)
		{
			real /=10;
			exp++;
		}

	}
	// convert to base 10 by recursive multiplication
	else if((real >0)&&(real <1))
	{
		while((real >0)&&(real <1))
		{
			real *=10;
			exp--;
		}
	}

#if USE_ROUNDING
	round = 0.5/pow(10, dig);
	real += round;
	if(real >= 10)
	{
		real/=10;
		exp++;
	}
#endif

	// get whole number part
	num = (Uint16)real;
	putud_com(num);

	// Get fractional part if needed
	if((dig > 0) && (dig <= FLT_DIG))
	{
		com_tx_fifo_push('.');
		fract = (Uint32)((real-num)*pow(10,dig));
		putfract_com(fract,dig);
	}

	// display exponent
	com_tx_fifo_push('e');
	putd_com(exp);

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Convert 32-bit float into engineering notation

Rounding and input conditioning determined by USE_ROUNDING and
INPUT_CONDITIONING definitions.

\author L.Baker
\par History:
\li	24/10/14 LB - initial creation
*/
void puteng_com(float32 real,uint16_t dig)
{
	int16 exp=0;
	double num=0;
	double fract;

	if(real < 0)
	{
		com_tx_fifo_push('-');
	}

	real =fabs(real);

#ifdef INPUT_CONDITIONING
	// check for NaN and overflow
	if(isnan(real))
	{
		puts_com((char*)"NaN");
		return;
	}
	// check for infinity
	if(isinf(real))
	{
		puts_com((char*)"INF");
		return;
	}
	// limit precision
	if(dig > FLT_DIG) dig=FLT_DIG;
#endif

	// convert to base 10 by recursive division
	if(real >= 1000)
	{
		while(real >=1000)
		{
			real /=1000.0;
			exp+=3;
		}

	}
	// convert to base 10 by recursive multiplication
	else if((real >0)&&(real <1))
	{
		while((real >0)&&(real <1))
		{
			real *=1000;
			exp-=3;
		}
	}

#if USE_ROUNDING
	round = 0.5/pow(10, dig);
	real += round;
#endif

	// get whole number part
	fract = modf(real,&num);
	putud_com((Uint16)num);


	// keep precision constant
	if(real >100)
		dig--;
	if(real >10)
		dig--;

	// Get fractional part if needed
	if((dig > 0) && (dig <= FLT_DIG))
	{
		com_tx_fifo_push('.');
		fract = fract*pow(10,dig);
		putfract_com((Uint32)fract,dig);
	}

	// display exponent
	com_tx_fifo_push('e');
	putd_com(exp);

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Convert 32-bit float as decimal

Rounding and input conditioning determined by USE_ROUNDING and
INPUT_CONDITIONING definitions.

\author L.Baker
\par History:
\li	24/10/14 LB - initial creation
\li 30/12/14 SP - no decimal point for dec=0
*/
void putf32_com(float32 real,uint16_t dec)
{
	//Number of decimal places to print.
	//# define NO_OF_PLACES 1
	
	double fract,num;
	if(real < 0)
	{
		com_tx_fifo_push('-');
	}

	real =fabs(real);

#ifdef INPUT_CONDITIONING
	// check for NaN and overflow
	if(isnan(real))
	{
		puts_com((char*)"NaN");
		return;
	}
	// check for infinity
	if(isinf(real))
	{
		puts_com((char*)"INF");
		return;
	}
#endif

	// If the number fit into a 32-bit int.
	if(real < 2^32)
	{
		fract = modf(real,&num);
		putul_com((Uint32)num);
		if(dec > 0)
		{
			com_tx_fifo_push('.');
			fract = fract*pow(10,dec);
			putfract_com((Uint32)fract,dec);
		}
	}
	// Use scientific notation
	else
	{
		putsci_com(real,FLT_DIG);
	}
}

// end serial_com.c
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
