/**
\file
\brief Definition of hardware constants and IO macros

\author S.Parker
\par History:
\li	21/02/15 SP - initial creation
*/
#ifndef HARDWARE_H_
#define HARDWARE_H_

/* =========================================================================
__Definitions()
============================================================================ */

/** @name IO macros with functionality */
//@{
#define LED1_SET() GpioDataRegs.GPBSET.bit.GPIO32=1
#define LED1_CLEAR() GpioDataRegs.GPBCLEAR.bit.GPIO32=1
#define LED1_TOGGLE() GpioDataRegs.GPBTOGGLE.bit.GPIO32=1

#define LED2_SET() GpioDataRegs.GPBSET.bit.GPIO33=1
#define LED2_CLEAR() GpioDataRegs.GPBCLEAR.bit.GPIO33=1
#define LED2_TOGGLE() GpioDataRegs.GPBTOGGLE.bit.GPIO33=1

#define FAN1_ON() GpioDataRegs.GPASET.bit.GPIO14=1
#define FAN1_OFF() GpioDataRegs.GPACLEAR.bit.GPIO14=1

#define FAN2_ON() GpioDataRegs.GPASET.bit.GPIO9=1
#define FAN2_OFF() GpioDataRegs.GPACLEAR.bit.GPIO9=1

#define SW_CLOSED() (GpioDataRegs.GPADAT.bit.GPIO22==0)
#define SW_OPEN() (GpioDataRegs.GPADAT.bit.GPIO22==1)

#define REM_CLOSED() (GpioDataRegs.GPADAT.bit.GPIO24==0)
#define REM_OPEN() (GpioDataRegs.GPADAT.bit.GPIO24==1)
//@}

/** @name Protection levels */
//@{
///Comparator protection
#define IBOOST_OC_COMP	827 // counts (9A)
#define IAC_OC_COMP		442	// counts (TBD)
#define IAC_OC_COMP_ZERO 512 // counts - zero A, configure 2 comparators for +-

///Software protection
#define IBOOST_OC_SOFT	8 // A
#define IAC_OC_SOFT		15 // A
#define VDC_OV_SOFT		600 // V
//@}

/** @name Analogue scalling factors, including manual calibration */
//@{
#define	IBOOST_SCALE	(0.0027191162109375/0.9649) // A/count

#define	IAC_SCALE		0.0101725260416667 // TBD A/count
#define IAC_OFFSET		20.833333333333333 // TBD A

#define VIN_SCALE		0.1962515024038460 // V/count

#define	VDC_SCALE		0.1962515024038460 // V/count

#define VOUT_SCALE		(0.2770317925347220/1.0012) // V/count
#define VOUT_OFFSET		(527.7777777778-3.1056) // V
//@}

#endif /* HARDWARE_H_ */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
