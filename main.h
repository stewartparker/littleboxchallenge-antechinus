/**
\file
\brief  Header for code entry point, background timer and serial interface

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
\li 29/12/14 SP - display and keyboard framework
\li 31/12/14 SP - grab code
*/
#ifndef MAIN_H_
#define MAIN_H_

/* =========================================================================
__Definitions()
============================================================================ */

/** @name Display definitions */
//@{
#define ANALOGUE_DISPLAY_START 0xC000
#define HELP_DISPLAY_START 0xD000
#define GRAB_DISPLAY_START 0xE000
#define IDLE_DISPLAY_STATE 0xF000
//@}

/** @name Grab definitions */
//@{
/// Choose type of variable to grab (only 1)
//#define GRAB_INT16
//#define GRAB_UINT16
//#define GRAB_INT32
#define GRAB_FLOAT

/// How many of each variable to record
#define GRAB_LENGTH 340
/// How many different variables to record
#define GRAB_WIDTH 6
/// How many to skip between taking records
#define GRAB_DECIM 0

/// Grab state control
#define GRAB_STATE_WAIT		0
#define GRAB_STATE_LOG		1
#define GRAB_STATE_PRINT	2
//@}

/* =========================================================================
__Typedefs()
============================================================================ */

/// Interrupt counters
typedef struct
{
	uint32_t
		powerstage_isr,
		cla_task1_isr;
} type_int_counter;

/// Grab state management variables
typedef struct
{
	uint16_t
		state,
		count,
		decim_count,
		log_flag,
		log_done_flag;
} type_grab;

/* =========================================================================
__Exported_Variables()
============================================================================ */

extern type_int_counter interrupt_counter;

/** Boot ROM sine table starts at 0x003FDF00 and has 641 entries of 32 bit sine
	values making up one and a quarter periods (plus one entry). For 16 bit
	values, use just the high word of the 32 bit entry. Peak value is 0x40000000 */
extern int16
	*sin_table_fixed, *cos_table_fixed;

/** Boot ROM sine table starts at 0x003FD860 and has 641 entries of single precision
	values making up one and a quarter periods (plus one entry). */
extern float32
	sin_table_float[641],
	*cos_table_float;

/// Pointer to location of the sine table in RIM
extern Uint16 *sin_table_float_ROM;

/** @name Grab varibles */
//@{
extern type_grab grab;

#ifdef GRAB_INT16
	extern int16 grab_table[GRAB_LENGTH][GRAB_WIDTH];
#endif

#ifdef GRAB_UINT16
	extern Uint16 grab_table[GRAB_LENGTH][GRAB_WIDTH];
#endif

#ifdef GRAB_INT32
	extern int32 grab_table[GRAB_LENGTH][GRAB_WIDTH];
#endif

#ifdef GRAB_FLOAT
	extern float32 grab_table[GRAB_LENGTH][GRAB_WIDTH];
	extern float32 cla2cpu_grab_data[GRAB_WIDTH];
#endif
//@}

/* =========================================================================
__Exported_Functions()
============================================================================ */



#endif /* MAIN_H_ */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
