/**
\file
\brief Power stage setup

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
\li 29/12/14 SP - match rev 1 pin usage
*/

// compiler standard include files
#include <stdint.h>

// processor standard include files
#include "F2806x_Device.h"
#include "F2806x_GlobalPrototypes.h"

// local include files
#include "power_stage.h"
#include "hardware.h"
#include "fault.h"

/*  =========================================================================
__Definitions()
============================================================================ */

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Variables()
============================================================================ */

/* =========================================================================
__Local_Function_Prototypes()
============================================================================ */

/// ePWM4 trip interrupt - overcurrent triggered
#pragma CODE_SECTION(epwm4_trip_isr, "ramfuncs");
interrupt void epwm4_trip_isr(void);

/// ePWM1 trip interrupt - overcurrent triggered
#pragma CODE_SECTION(epwm1_trip_isr, "ramfuncs");
interrupt void epwm1_trip_isr(void);

/* =========================================================================
__Local_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
ePWM1 Trip Routine (for IAC overcurrent)

\author S.Parker
\par History:
\li	30/12/14 SP - Initial Creation
*/
interrupt void epwm1_trip_isr(void)
{
	// Ensure switches are all off
	SWITCHING_DISABLE();

	// Record IAC overcurrent event
	FAULT_SET(FAULT_IAC_OC);

	// Ackowledge interrupt in PIE
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;
	// EPWM TZ interrupt is cleared in fault handling
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
ePWM4 Trip Routine (for IBOOST overcurrent)

\author S.Parker
\par History:
\li	30/12/14 SP - Initial Creation
*/
interrupt void epwm4_trip_isr(void)
{
	// Ensure switches are all off
	SWITCHING_DISABLE();

	// Record IBOOST overcurrent event
	FAULT_SET(FAULT_IBOOST_OC);

	// Ackowledge interrupt in PIE
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;
	// EPWM TZ interrupt is cleared in fault handling
}

/* =========================================================================
__Exported_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialises ePWM 4 for boost operation

\author S.Parker
\par History:
\li	03/10/14 SP - Initial Creation
\li	29/12/14 SP - To match rev 1 pins
*/
void InitBoost(void)
{
	EALLOW;
	SysCtrlRegs.PCLKCR1.bit.EPWM4ENCLK = 1;  // ePWM4
	EDIS;

	/* Setup ePWM4 */
	// Time-base registers
	EPwm4Regs.TBPRD = BOOST_PERIOD;       // Set timer period
	EPwm4Regs.TBPHS.half.TBPHS = 0;		// Time-Base Phase Register
	EPwm4Regs.TBCTR = 0;			// Time-Base Counter Register

	EPwm4Regs.TBCTL.bit.PHSDIR = 1; // Count up after sync
	EPwm4Regs.TBCTL.bit.PRDLD = 0;
	EPwm4Regs.TBCTL.bit.CTRMODE = 2; // Count-up/down mode
	//EPwm4Regs.TBCTL.bit.PHSEN = 1; // Load counter phase on sync
	EPwm4Regs.TBCTL.bit.SYNCOSEL = 0; // sync out pass through
	EPwm4Regs.TBCTL.bit.HSPCLKDIV = 0;
	EPwm4Regs.TBCTL.bit.CLKDIV = 0;

	// Interrupt registers
	EPwm4Regs.ETSEL.bit.INTSEL = 1; // Interrupt on counter zero
	EPwm4Regs.ETSEL.bit.INTEN = 1; // enable int generation
	EPwm4Regs.ETPS.bit.INTPRD = 1; // every time
	EPwm4Regs.ETCLR.bit.INT = 1;  // clear any pending
	// SOC for ADC registers
	EPwm4Regs.ETSEL.bit.SOCASEL = 1; // start ADC on counter zero
	EPwm4Regs.ETSEL.bit.SOCAEN = 1;
	EPwm4Regs.ETPS.bit.SOCAPRD = 1;

	// Setup shadow register load on ZERO
	EPwm4Regs.CMPCTL.bit.SHDWAMODE = 0; // Compare shadow enabled
	EPwm4Regs.CMPCTL.bit.SHDWBMODE = 0; // Compare shadow enabled
	EPwm4Regs.CMPCTL.bit.LOADAMODE = 2;	// load on zero or period
	EPwm4Regs.CMPCTL.bit.LOADBMODE = 2;	// load on zero or period

	// Set compare values
	EPwm4Regs.CMPA.half.CMPA = BOOST_PERIOD;    // Set duty 100% initially

	// Set actions
	EPwm4Regs.AQCTLA.bit.CAU = 1; // Clear on Up-Count
	EPwm4Regs.AQCTLA.bit.CAD = 2; // Set on Down-Count

	// DeadBand configuration
	EPwm4Regs.DBCTL.bit.HALFCYCLE = 1; // 2xTBCLK
	EPwm4Regs.DBCTL.bit.IN_MODE = 0; // EPWM2A is the source for both falling-edge and rising-edge delay
	EPwm4Regs.DBCTL.bit.OUT_MODE = 3;  // Full DB
	EPwm4Regs.DBCTL.bit.POLSEL = 2; 		// Active High Complementary (AHC)
	EPwm4Regs.DBRED = 18;		// Set DeadBand time for output A = 9/90e6/2 = 50ns
	EPwm4Regs.DBFED = 18;		// Set DeadBand time for output B = 9/90e6/2 = 50ns

	/* Setup trip event on Comp1 (Iboost) */
	// Map interrupt and enable in PIE
	EALLOW;
	PieVectTable.EPWM4_TZINT = &epwm4_trip_isr;
	EDIS;
	IER |= M_INT2;
	PieCtrlRegs.PIEIER2.bit.INTx4 = 1;

	EALLOW;
	// Define event A (DCAEVT1)
	EPwm4Regs.DCTRIPSEL.bit.DCAHCOMPSEL = 8; // DCAH = Comparator 1 output
	EPwm4Regs.DCTRIPSEL.bit.DCALCOMPSEL = 0; // DCAL = TZ1
	EPwm4Regs.DCACTL.bit.EVT1SRCSEL = 0; // DCAEVT1 = DCAEVT1 (not filtered)
	EPwm4Regs.DCACTL.bit.EVT1FRCSYNCSEL = 1; // Take async path

	// Use DCAEVT1 and define action
	EPwm4Regs.TZDCSEL.bit.DCAEVT1 = 2; // DCAEVT1 will become active as Comp1 goes high (DCAL don't care)
	EPwm4Regs.TZSEL.bit.DCAEVT1 = 1; // Enable DCAEVT1 as one shot trip sources
	EPwm4Regs.TZEINT.bit.OST = 1; // Enable TZ interrupt
	EPwm4Regs.TZCTL.bit.TZA = 2; // On trip force EPWM4A low
	EPwm4Regs.TZCTL.bit.TZB = 2; // On trip force EPWM4B low

	EPwm4Regs.TZCLR.all = 0x000D; // Seemed to work better when clearing all 3 flags at once
	EDIS;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialises ePWM 1 and 2 for H-bridge operation

\author S.Parker
\par History:
\li	03/10/14 SP - Initial Creation
\li	29/12/14 SP - To match rev 1 pins
*/
void InitHBridge(void)
{
	EALLOW;
	SysCtrlRegs.PCLKCR1.bit.EPWM1ENCLK = 1;  // ePWM1
	SysCtrlRegs.PCLKCR1.bit.EPWM2ENCLK = 1;  // ePWM2
	EDIS;

	/* Setup ePWM1 */
	// Time-base registers
	EPwm1Regs.TBPRD = HBRIDGE_PERIOD;       // Set timer period
	EPwm1Regs.TBPHS.half.TBPHS = 0;		// Time-Base Phase Register
	EPwm1Regs.TBCTR = 0;			// Time-Base Counter Register

	EPwm1Regs.TBCTL.bit.PHSDIR = 1; // Count up after sync
	EPwm1Regs.TBCTL.bit.PRDLD = 0;
	EPwm1Regs.TBCTL.bit.CTRMODE = 2; // Count-up/down mode
	EPwm1Regs.TBCTL.bit.PHSEN = 0; // Do not load counter phase on sync (master)
	//EPwm1Regs.TBCTL.bit.SYNCOSEL = 1; // Generate sync on zero
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = 0;
	EPwm1Regs.TBCTL.bit.CLKDIV = 0;

	// Setup shadow register load on ZERO and period
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = 0; // Compare shadow enabled
	EPwm1Regs.CMPCTL.bit.SHDWBMODE = 0; // Compare shadow enabled
	EPwm1Regs.CMPCTL.bit.LOADAMODE = 2;	// load on zero or period
	EPwm1Regs.CMPCTL.bit.LOADBMODE = 2;	// load on zero or period

	// Set compare values
	EPwm1Regs.CMPA.half.CMPA = HBRIDGE_PERIOD_2;    // Set duty 50% initially

	// Set actions
	EPwm1Regs.AQCTLA.bit.CAU = 1;
	EPwm1Regs.AQCTLA.bit.CAD = 2;

	// DeadBand configuration
	EPwm1Regs.DBCTL.bit.HALFCYCLE = 1; // 2xTBCLK
	EPwm1Regs.DBCTL.bit.IN_MODE = 0; // EPWM2A is the source for both falling-edge and rising-edge delay
	EPwm1Regs.DBCTL.bit.OUT_MODE = 3;  // Full DB
	EPwm1Regs.DBCTL.bit.POLSEL = 2; 		// Active High Complementary (AHC)
	EPwm1Regs.DBRED = 18;		// Set DeadBand time for output A = 9/90e6/2 = 50ns
	EPwm1Regs.DBFED = 18;		// Set DeadBand time for output B = 9/90e6/2 = 50ns

	/* Setup trip event on Comp2 (IAC positive) and Comp3 (IAC negative) */
	// Map interrupt and enable in PIE
	EALLOW;
	PieVectTable.EPWM1_TZINT = &epwm1_trip_isr;
	PieCtrlRegs.PIEIER2.bit.INTx1 = 1;
	EDIS;
	IER |= M_INT2;

	EALLOW;
/*	// Define event A (DCAEVT1)
	EPwm1Regs.DCTRIPSEL.bit.DCAHCOMPSEL = 9; // DCAH = Comparator 2 output
	EPwm1Regs.DCTRIPSEL.bit.DCALCOMPSEL = 0; // DCAL = TZ1
	EPwm1Regs.DCACTL.bit.EVT1SRCSEL = 0; // DCAEVT1 = DCAEVT1 (not filtered)
	EPwm1Regs.DCACTL.bit.EVT1FRCSYNCSEL = 1; // Take async path

	// Define event B (DCBEVT1)
	EPwm1Regs.DCTRIPSEL.bit.DCBHCOMPSEL = 10; // DCBH = Comparator 3 output
	EPwm1Regs.DCTRIPSEL.bit.DCBLCOMPSEL = 0; // DCBL = TZ1 (not used)
	EPwm1Regs.DCBCTL.bit.EVT1SRCSEL = 0; // DCBEVT1 = DCBEVT1 (not filtered)
	EPwm1Regs.DCBCTL.bit.EVT1FRCSYNCSEL = 1; // Take async path

	// Use DCAEVT1 and DCBEVT1
	EPwm1Regs.TZDCSEL.bit.DCAEVT1 = 2; // DCAEVT1 will become active as Comp2/DCAH goes high (DCAL don't care)
	EPwm1Regs.TZDCSEL.bit.DCBEVT1 = 2; // DCBEVT1 will become active as Comp3/DCBH goes high (DCAL don't care)
	EPwm1Regs.TZSEL.bit.DCAEVT1 = 1; // Enable DCAEVT1 as one shot trip sources
	EPwm1Regs.TZSEL.bit.DCBEVT1 = 1; // Enable DCBEVT1 as one shot trip sources

	// Define trip action
	EPwm1Regs.TZEINT.bit.OST = 1; // Enable TZ interrupt
	EPwm1Regs.TZCTL.bit.TZA = 2; // On trip force EPWM2A low
	EPwm1Regs.TZCTL.bit.TZB = 1; // On trip force EPWM2B high
*/
	EPwm1Regs.TZCLR.all = 0x002D; // Seemed to work better when clearing all 3 flags at once
	EDIS;

	/* Setup ePWM2 */
	// Time-base registers
	EPwm2Regs.TBPRD = HBRIDGE_PERIOD;       		   // Set timer period, PWM frequency = 1 / period
	EPwm2Regs.TBPHS.half.TBPHS = 0;			// Time-Base Phase Register
	EPwm2Regs.TBCTR = 0;					   // Time-Base Counter Register

	EPwm2Regs.TBCTL.bit.PHSDIR = 1; // Count up after sync
	EPwm2Regs.TBCTL.bit.PRDLD = 0;
	EPwm2Regs.TBCTL.bit.CTRMODE = 2; // Count-up/down mode
	//EPwm2Regs.TBCTL.bit.PHSEN = 1; // Load counter phase on sync
	EPwm2Regs.TBCTL.bit.SYNCOSEL = 0; // Pass through sync_in->sync_out
	EPwm2Regs.TBCTL.bit.HSPCLKDIV = 0;
	EPwm2Regs.TBCTL.bit.CLKDIV = 0;

	// Setup shadow register load on ZERO
	EPwm2Regs.CMPCTL.bit.SHDWAMODE = 0; // Compare shadow enabled
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = 0; // Compare shadow enabled
	EPwm2Regs.CMPCTL.bit.LOADAMODE = 2;	// load on counter Zero
	EPwm2Regs.CMPCTL.bit.LOADBMODE = 2;	// load on counter Zero

	// Set compare values
	EPwm2Regs.CMPA.half.CMPA = HBRIDGE_PERIOD_2;    // Set duty 50% initially

	// Set actions
	EPwm2Regs.AQCTLA.bit.CAU = 1;
	EPwm2Regs.AQCTLA.bit.CAD = 2;

	// DeadBand configuration
	EPwm2Regs.DBCTL.bit.HALFCYCLE = 1; // 2xTBCLK
	EPwm2Regs.DBCTL.bit.IN_MODE = 0; // EPWM2A is the source for both falling-edge and rising-edge delay
	EPwm2Regs.DBCTL.bit.OUT_MODE = 3;  // Full DB
	EPwm2Regs.DBCTL.bit.POLSEL = 2; 		// Active High Complementary (AHC)
	EPwm2Regs.DBRED = 18;		// Set DeadBand time for output A = 9/90e6/2 = 50ns
	EPwm2Regs.DBFED = 18;		// Set DeadBand time for output B = 9/90e6/2 = 50ns

	EALLOW;
/*	// Define event A (DCAEVT1)
	EPwm2Regs.DCTRIPSEL.bit.DCAHCOMPSEL = 9; // DCAH = Comparator 2 output
	EPwm2Regs.DCTRIPSEL.bit.DCALCOMPSEL = 0; // DCAL = TZ1 (not used)
	EPwm2Regs.DCACTL.bit.EVT1SRCSEL = 0; // DCAEVT1 = DCAEVT1 (not filtered)
	EPwm2Regs.DCACTL.bit.EVT1FRCSYNCSEL = 1; // Take async path

	// Define event B (DCBEVT1)
	EPwm2Regs.DCTRIPSEL.bit.DCBHCOMPSEL = 10; // DCBH = Comparator 3 output
	EPwm2Regs.DCTRIPSEL.bit.DCBLCOMPSEL = 0; // DCBL = TZ1 (not used)
	EPwm2Regs.DCBCTL.bit.EVT1SRCSEL = 0; // DCBEVT1 = DCBEVT1 (not filtered)
	EPwm2Regs.DCBCTL.bit.EVT1FRCSYNCSEL = 1; // Take async path

	// Use DCAEVT1 and DCBEVT1
	EPwm2Regs.TZDCSEL.bit.DCAEVT1 = 2; // DCAEVT1 will become active as Comp2/DCAH goes high (DCAL don't care)
	EPwm2Regs.TZDCSEL.bit.DCBEVT1 = 2; // DCBEVT1 will become active as Comp3/DCBH goes high (DCAL don't care)
	EPwm2Regs.TZSEL.bit.DCAEVT1 = 1; // Enable DCAEVT1 as one shot trip sources
	EPwm2Regs.TZSEL.bit.DCBEVT1 = 1; // Enable DCBEVT1 as one shot trip sources

	// Define trip action
	//EPwm2Regs.TZEINT.bit.OST = 1; // Enable TZ interrupt (on ePWM1)
	EPwm2Regs.TZCTL.bit.TZA = 2; // On trip force EPWM2A low
	EPwm2Regs.TZCTL.bit.TZB = 2; // On trip force EPWM2B low
*/
	EPwm2Regs.TZCLR.all = 0x002D; // Seemed to work better when clearing all 3 flags at once
	EDIS;
}

// end power_stage.c
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
