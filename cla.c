/**
\file
\brief Initialisation of the CLA

The CLA is used to run the main feedback control loops and modulation.

\author S.Parker
\par History:
\li	18/10/14 SP - initial creation
*/

// compiler standard include files
#include <stdint.h>

// processor standard include files
#include "F2806x_Device.h"
#include "F2806x_GlobalPrototypes.h"

// local include files
#include "main.h"
#include "analogue.h"
#include "power_stage.h"
#include "fsm.h"
#include "cla.h"

/*  =========================================================================
__Definitions()
============================================================================ */

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Variables()
============================================================================ */

/** @name Constants from specific memory locations */
//@{
// Used to load CLA code into RAM
extern Uint16 ClafuncsLoadStart, ClafuncsLoadEnd, ClafuncsRunStart, ClaProg_Start;

// Sinewave generation
#pragma DATA_SECTION(CLA_sin_table,"CLAsinetable");
float32 CLA_sin_table[641],
		*CLA_cos_table;
//@}

/** @name CLA-CPU shared variables */
//@{
#ifdef PSIM_VERSION // begin PSIM_VERSION {1}
type_cla2cpu
	cla2cpu =
	{
			{0,0,0},	// adc.iboost
			{0,0,0},
			{0,0,0},
			{0,0,0},
		{	// phase
			0,	// current
			0	// step
		}
	};
type_cpu2cla
	cpu2cla =
	{
		{0,0,0},	// adc.vzero
		{0,0,0},	// adc.vdc_average
		0, // adc.average_flag
		{
			0,	// openloop.boost_dutycycle
			0	// openloop.hbridge_moddepth
		}
	};
#else	// not PSIM_VERSION {1}

/// Variables to transfer from CLA to CPU
#pragma DATA_SECTION(cla2cpu,"ClaToCpuMsgRAM");
type_cla2cpu
	cla2cpu =
	{
		{
			{0,0,0},	// adc.iboost
			{0,0,0},
			{0,0,0},
			{0,0,0}
		},	
		{	// phase
			0,	// current
			0	// step
		}
	};

/// Variables to transfer from CPU to CLA
#pragma DATA_SECTION(cpu2cla,"CpuToClaMsgRAM");
type_cpu2cla
	cpu2cla =
	{
		{
			{0,0,0},	// adc.vzero
			{0,0,0},	// adc.vdc_average
			0 // adc.average_flag
		},	
		{
			0,	// hbridge.moddepth
			0	// hbridge.voltage
		},
		{
			0,	// boost.dutycycle
			0,	// boost.current
			0	// boost.voltage
		}
	};	

#endif // end PSIM_VERSION {1}
//@}

/* =========================================================================
__Global_Function_Prototypes()
============================================================================ */

/// Copy memory from one location to another
void MemCopy(uint16_t *SourceAddr, uint16_t* SourceEndAddr, uint16_t* DestAddr);

/* =========================================================================
__Local_Function_Prototypes()
============================================================================ */

/// ISR to run on completion of CLA task 1
#pragma CODE_SECTION(cla_task1_isr, "ramfuncs");
interrupt void cla_task1_isr(void);

/* =========================================================================
__Exported_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialises the CLA

\author S.Parker
\par History:
\li	18/10/14 SP - initial creation
*/
void InitCLA(void)
{
	EALLOW;
	SysCtrlRegs.PCLKCR3.bit.CLA1ENCLK = 1;
	EDIS;

	EALLOW;
	/* Setup PIE for CLA interrupts */
	// CLA1_INT1 interrupt vector
	PieVectTable.CLA1_INT1 = &cla_task1_isr;

	// CLA1_INT1 in PIE
	PieCtrlRegs.PIEIER11.bit.INTx1 = 1;
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP11;
	EDIS;

	// CPU Interrupt mask for Group 11
	IER |= M_INT11;
	
#ifndef PSIM_VERSION // begin PSIM_VERSION {2}

	// Copy CLA tasks into RAM
	MemCopy(&ClafuncsLoadStart, &ClafuncsLoadEnd, &ClafuncsRunStart);
	
	// Copy sine table into CLA RAM
	MemCopy(&sin_table_float_ROM[0], &sin_table_float_ROM[1282], (Uint16*)&CLA_sin_table[0]);
	
#endif // end PSIM_VERSION {2}	
	
	CLA_cos_table = &CLA_sin_table[128];
	
	
	
	EALLOW;
	// Compute all CLA task vectors
	Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bol_Hol - (Uint32)&ClaProg_Start);
	Cla1Regs.MVECT3 = (Uint16)((Uint32)&ClaTask3_InitCLA - (Uint32)&ClaProg_Start);

	// Map CLA tasks to events and enable tasks
	Cla1Regs.MPISRCSEL1.bit.PERINT1SEL = 0; //ADCINT1 triggers task 1
	Cla1Regs.MIER.bit.INT1 = 1;
	Cla1Regs.MICLR.bit.INT1 = 1;

	Cla1Regs.MPISRCSEL1.bit.PERINT3SEL = 1; //No hardware interrupt source for task 3
	Cla1Regs.MIER.bit.INT3 = 1;
	Cla1Regs.MICLR.bit.INT3 = 1;


	/* Switch the CLA program space to the CLA
	 * Also switch over CLA Data RAM 0 and 1 (RAML1+L2)
	 * CAUTION: The RAMxCPUE bits can only be enabled by writing to the register
	 * and not the individual bit field. Furthermore, the status of these bitfields
	 * is not reflected in either the watch or register views - they always read as 
	 * zeros. This is a known bug and the user is advised to test CPU accessibilty
	 * first before proceeding
	 */
	Cla1Regs.MMEMCFG.all = BIT0|BIT4|BIT5|BIT8|BIT9;
	Cla1Regs.MCTL.bit.IACKE = 1; //  Allows CPU to start CLA tasks
	
	EDIS;
	
	/* Setup timing pin */
	EALLOW;
	GpioCtrlRegs.GPAMUX1.all |= 0x00100000; // ePWM6A

	SysCtrlRegs.PCLKCR1.bit.EPWM6ENCLK = 1;

	EPwm6Regs.AQSFRC.bit.RLDCSF = 3; // Load software force immediately
	EPwm6Regs.AQCSFRC.bit.CSFA = 1; // force a continuous low on ePWM6A
	EDIS;

	// Initialise cla variables by forcing task 3
	Cla1ForceTask3andWait();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Maps the correct CLA Task1 for the selected operating mode

\author S.Parker
\par History:
\li	18/10/14 SP - initial creation
*/
void map_cla_task1(void)
{
	EALLOW;
	
	// Disable CLA operation
	Cla1Regs.MIER.bit.INT1 = 0;

	if(is_boost_openloop()&&is_hbridge_open_loop())
	{
		Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bol_Hol -(Uint32)&ClaProg_Start);
	}
	else if(is_boost_currentreg()&&is_hbridge_open_loop())
	{
		Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bcr_Hol -(Uint32)&ClaProg_Start);
	}
	else if(is_boost_busreg()&&is_hbridge_open_loop())
	{
		Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bbr_Hol -(Uint32)&ClaProg_Start);
	}
	else if(is_boost_openloop()&&is_hbridge_dc_bus_comp())
	{
		Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bol_Hdc -(Uint32)&ClaProg_Start);
	}
	else if(is_boost_currentreg()&&is_hbridge_dc_bus_comp())
	{
		Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bcr_Hdc -(Uint32)&ClaProg_Start);
	}
	else if(is_boost_busreg()&&is_hbridge_dc_bus_comp())
	{
		Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bbr_Hdc -(Uint32)&ClaProg_Start);
	}
	else if(is_boost_none())
	{
		if(is_hbridge_open_loop())
		{
			Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bol_Hol -(Uint32)&ClaProg_Start);
		}
		else if(is_hbridge_dc_bus_comp())
		{
			Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bol_Hdc -(Uint32)&ClaProg_Start);
		}
	}
	else if(is_hbridge_none())
	{
		if(is_boost_openloop())
		{
			Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bol_Hol -(Uint32)&ClaProg_Start);
		}
		else if(is_boost_currentreg())
		{
			Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bcr_Hol -(Uint32)&ClaProg_Start);
		}
		else if(is_boost_busreg())
		{
			Cla1Regs.MVECT1 = (Uint16)((Uint32)&ClaTask1_Bbr_Hol -(Uint32)&ClaProg_Start);
		}
	}

	// Reset CLA variables
	Cla1ForceTask3andWait();

	// Enable CLA operation
	Cla1Regs.MICLR.bit.INT1 = 1;
	Cla1Regs.MIER.bit.INT1 = 1;
	
	EDIS;
}

/* =========================================================================
__Local_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
CLA task 1 triggers this interrupt
Logs grab values from CLA

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
interrupt void cla_task1_isr(void)
{
	/* Increment interrupt counter */
	interrupt_counter.cla_task1_isr++;

	/* Log grab data */
	// CPU or CLA grab data only (check powerstage.c)
#ifdef GRAB_FLOAT
	if(grab.state == GRAB_STATE_LOG)
	{
		if(grab.count < GRAB_LENGTH)
		{
			if(grab.decim_count == GRAB_DECIM)
			{
				grab_table[grab.count][0] = cla2cpu_grab_data[0];
				grab_table[grab.count][1] = cla2cpu_grab_data[1];
				grab_table[grab.count][2] = cla2cpu_grab_data[2];
				grab_table[grab.count][3] = cla2cpu_grab_data[3];
				grab_table[grab.count][4] = cla2cpu_grab_data[4];
				grab_table[grab.count][5] = cla2cpu_grab_data[5];
				grab.count++;
			}

			if(grab.decim_count == 0)
				grab.decim_count = GRAB_DECIM;
			else
				grab.decim_count--;
		}
		else
		{
			grab.log_done_flag = 1;
		}
	}
#endif

	// Ackowledge interrupt in PIE
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP11;
}

// end cla.c
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
