/**
\file
\brief HBridge state machine functions

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
\li 12/06/15 SP - split boost and hbridge
*/

// compiler standard include files
#include <stdint.h>

// processor standard include files
#include "F2806x_Device.h"
#include "F2806x_GlobalPrototypes.h"

// local include files
#include "power_stage.h"
#include "fsm.h"
#include "cla.h"
#include "fault.h"

/*  =========================================================================
__Definitions()
============================================================================ */

/// Fudge factor to correct RMS output voltage error due to deadtime
#define VRMS_FUDGE 1.025

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Local_Function_Prototypes()
============================================================================ */

/** @name State Machine Functions */
//@{
void hbridge_state_init(void);
void hbridge_state_stop(void);
void hbridge_state_open_loop_hold(void);
void hbridge_state_open_loop_ramp(void);
void hbridge_state_open_loop_run(void);
void hbridge_state_DC_bus_comp_hold(void);
void hbridge_state_DC_bus_comp_ramp(void);
void hbridge_state_DC_bus_comp_run(void);
void hbridge_state_fault(void);
//@}

/* =========================================================================
__Variables()
============================================================================ */

/// State machine pointer and variables
type_state hbridge_state = { &hbridge_state_init, (statePtr)0, 1, 0 };

/* =========================================================================
__Local_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialisation State
- Performs power stage initialisation

\author S.Parker
\par History:
\li	29/12/14 SP - initial creation
*/
void hbridge_state_init(void)
{
	if(hbridge_state.first_time == 1)
	{
		hbridge_state.state_name = "Init  ";
		hbridge_state.first_time = 0;

		SWITCHING_DISABLE_HBRIDGE();

		background.hbridge.init = 0;

		// Prevent accidental queuing transistion to run state
		background.hbridge.enable = 0;
	}

	// Initialise interrupt variables
	background.hbridge.moddepth = DEFAULT_MOD_DEPTH;
	cpu2cla.hbridge.moddepth = (float32)background.hbridge.moddepth*0.01;

	/* State transitions */

	hbridge_state.current = (statePtr)&hbridge_state_stop;
	return;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Waits for mode setting

\author S.Parker
\par History:
\li	12/06/15 SP - initial creation
*/
void hbridge_state_stop(void)
{
	if(hbridge_state.first_time == 1)
	{
		hbridge_state.state_name = "Stop  ";
		hbridge_state.first_time = 0;

		SWITCHING_DISABLE_HBRIDGE();
		
		background.hbridge.init = 1;
	}

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		hbridge_state.current = (statePtr)&hbridge_state_fault;
		return;
	}

	if( background.hbridge.mode == HBRIDGE_MODE_OPEN_LOOP )
	{
		// Enable hbridge only
		hbridge_state.current = (statePtr)&hbridge_state_open_loop_hold;
		return;
	}
	if( background.hbridge.mode == HBRIDGE_MODE_DC_BUS_COMP )
	{
		// Enable hbridge only
		hbridge_state.current = (statePtr)&hbridge_state_DC_bus_comp_hold;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Wait for enable

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void hbridge_state_open_loop_hold(void)
{

	if(hbridge_state.first_time == 1)
	{
		hbridge_state.state_name = "OL Hold";
		hbridge_state.first_time = 0;

		SWITCHING_DISABLE_HBRIDGE();
	}

	// Zero out interrupt variables
	cpu2cla.hbridge.moddepth = 0.0;

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		hbridge_state.current = (statePtr)&hbridge_state_fault;
		return;
	}
	
	// State transition for exit open loop
	if(background.hbridge.mode != HBRIDGE_MODE_OPEN_LOOP)
	{
		hbridge_state.current = (statePtr)&hbridge_state_stop;
		return;
	}

	// State transition for enable
	if(background.hbridge.enable == 1)
	{
		cpu2cla.hbridge.moddepth = DEFAULT_MOD_DEPTH;
		hbridge_state.current = (statePtr)&hbridge_state_open_loop_ramp;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Open Loop Ramp State - HBridge
- Ramp modulation

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void hbridge_state_open_loop_ramp(void)
{
	static uint16_t ramp_done;

	if(hbridge_state.first_time == 1)
	{
		hbridge_state.state_name = "OL Ramp";
		hbridge_state.first_time = 0;

		ramp_done = 0;

		SWITCHING_ENABLE_HBRIDGE();
	}

	// Ramp interrupt commands by 1% per ms
	if( (cpu2cla.hbridge.moddepth-(float32)background.hbridge.moddepth*0.01) > 0.01 )
	{
		cpu2cla.hbridge.moddepth -= 0.01;
	}
	else if( (cpu2cla.hbridge.moddepth-(float32)background.hbridge.moddepth*0.01) < -0.01 )
	{
		cpu2cla.hbridge.moddepth += 0.01;
	}
	else
	{
		cpu2cla.hbridge.moddepth = (float32)background.hbridge.moddepth*0.01;
		ramp_done |= 2;
	}

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		hbridge_state.current = (statePtr)&hbridge_state_fault;
		return;
	}

	// State transition for disable
	if(background.hbridge.enable != 1)
	{
		hbridge_state.current = (statePtr)&hbridge_state_stop;
		return;
	}

	// State transition for ramping completed
	if(ramp_done == 2)
	{
		hbridge_state.current = (statePtr)&hbridge_state_open_loop_run;
		return;
	}

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Open Loop Run State - HBridge only
- Everything is good, output is enabled and running

\author S.Parker
\par History:
\li	07/02/15 SP - initial creation
*/
void hbridge_state_open_loop_run(void)
{
	if(hbridge_state.first_time == 1)
	{
		hbridge_state.state_name = "OL Run ";
		hbridge_state.first_time = 0;

		SWITCHING_ENABLE_HBRIDGE();
	}

	// Feed through command value to CLA
	cpu2cla.hbridge.moddepth = (float32)background.hbridge.moddepth*0.01;

	/* State transitions */
	
	// State transition for faults
	if(detected_faults != 0)
	{
		hbridge_state.current = (statePtr)&hbridge_state_fault;
		return;
	}

	// State transitions for disable
	if(background.hbridge.enable != 1)
	{
		hbridge_state.current = (statePtr)&hbridge_state_stop;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Open Loop with DC bus comp Hold State

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void hbridge_state_DC_bus_comp_hold(void)
{

	if(hbridge_state.first_time == 1)
	{
		hbridge_state.state_name = "DC Hold";
		hbridge_state.first_time = 0;

		cpu2cla.hbridge.voltage = 0.0;

		SWITCHING_DISABLE_HBRIDGE();
	}

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		hbridge_state.current = (statePtr)&hbridge_state_fault;
		return;
	}
	
	// State transition for exit open loop
	if(background.hbridge.mode != HBRIDGE_MODE_DC_BUS_COMP)
	{
		hbridge_state.current = (statePtr)&hbridge_state_stop;
		return;
	}

	// State transitions for enable
	if(background.hbridge.enable == 1)
	{
		cpu2cla.hbridge.voltage = 0.0;
		hbridge_state.current = (statePtr)&hbridge_state_DC_bus_comp_ramp;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Open Loop with DC bus comp Ramp State - HBridge
- Ramp modulation

\author S.Parker
\par History:
\li	15/06/15 SP - initial creation
*/
void hbridge_state_DC_bus_comp_ramp(void)
{
	static uint16_t ramp_done;

	if(hbridge_state.first_time == 1)
	{
		hbridge_state.state_name = "DC Ramp";
		hbridge_state.first_time = 0;

		ramp_done = 0;

		SWITCHING_ENABLE_HBRIDGE();
	}

	// Ramp interrupt commands by 3V per ms
	if( (cpu2cla.hbridge.voltage-((float32)background.hbridge.voltage*(float32)_SQRT2*(float32)VRMS_FUDGE)) > 3 )
	{
		cpu2cla.hbridge.voltage -= 3;
	}
	else if( (cpu2cla.hbridge.voltage-((float32)background.hbridge.voltage*(float32)_SQRT2)*(float32)VRMS_FUDGE) < -3 )
	{
		cpu2cla.hbridge.voltage += 3;
	}
	else
	{
		cpu2cla.hbridge.voltage = (float32)background.hbridge.voltage*(float32)_SQRT2*(float32)VRMS_FUDGE;
		ramp_done |= 2;
	}

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		hbridge_state.current = (statePtr)&hbridge_state_fault;
		return;
	}

	// State transition for disable
	if(background.hbridge.enable != 1)
	{
		hbridge_state.current = (statePtr)&hbridge_state_DC_bus_comp_hold;
		return;
	}

	// State transition for ramping completed
	if(ramp_done == 2)
	{
		hbridge_state.current = (statePtr)&hbridge_state_DC_bus_comp_run;
		return;
	}

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Open Loop with DC bus compRun State - HBridge only
- Everything is good, output is enabled and running

\author S.Parker
\par History:
\li	15/06/15 SP - initial creation
*/
void hbridge_state_DC_bus_comp_run(void)
{
	if(hbridge_state.first_time == 1)
	{
		hbridge_state.state_name = "DC Run ";
		hbridge_state.first_time = 0;

		SWITCHING_ENABLE_HBRIDGE();
	}

	// Feed through command value to CLA
	cpu2cla.hbridge.voltage = (float32)background.hbridge.voltage*(float32)_SQRT2*(float32)VRMS_FUDGE;

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		hbridge_state.current = (statePtr)&hbridge_state_fault;
		return;
	}

	// State transition for disable
	if(background.hbridge.enable != 1)
	{
		hbridge_state.current = (statePtr)&hbridge_state_DC_bus_comp_hold;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Fault State
- Performs fault handling (as yet undefined)

\author S.Parker
\par History:
\li	29/12/14 SP - initial creation
*/
void hbridge_state_fault(void)
{
	if(hbridge_state.first_time == 1)
	{
		hbridge_state.state_name = "Fault  ";
		hbridge_state.first_time = 0;

		SWITCHING_DISABLE_HBRIDGE();

		// Prevent queuing transistion to run state
		background.hbridge.enable = 0;
	}

	/* State transitions */

	// No faults remaining
	if(detected_faults == 0)
	{
		hbridge_state.current = (statePtr)&hbridge_state_stop;
		return;
	}
}

/* =========================================================================
__Exported_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Runs the current hbridge state function

\author S.Parker
\par History:
\li	12/06/15 SP - initial creation
*/
void run_hbridge_fsm(void)
{
	// Flag state transition
	hbridge_state.first_time = (hbridge_state.prev != hbridge_state.current);
	hbridge_state.prev = hbridge_state.current;

	// Call current state
	CALL_STATE(hbridge_state);
}
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Get hbridge operating mode

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
void set_hbridge_mode(uint16_t mode)
{
	if(is_boost_hold()&&is_hbridge_hold())
	{
		background.hbridge.mode = mode;
		map_cla_task1();
	}
}
int16_t get_hbridge_mode(void)
{
	return background.hbridge.mode;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Enable HBridge switching

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
void enable_hbridge(void)
{
	if( (hbridge_state.current == &hbridge_state_open_loop_hold)||
		(hbridge_state.current == &hbridge_state_DC_bus_comp_hold) )
	{
		background.hbridge.enable = 1;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Disable HBridge switching

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
void disable_hbridge(void)
{
	background.hbridge.enable = 0;
	SWITCHING_DISABLE_HBRIDGE();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns name of current state

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
char* get_hbridge_state_name(void)
{
	return hbridge_state.state_name;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Set open loop modulation depth

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
void set_hbridge_moddepth(int16_t md)
{
	if(md > 100) md = 100;
	if(md < -100) md = -100;

	background.hbridge.moddepth = md;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Get open loop modulation depth

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
float32 get_hbridge_moddepth(void)
{
	return background.hbridge.moddepth;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Set RMS target voltage

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
void set_hbridge_voltage(int16_t vv)
{
	if(vv > 250) vv = 250;
	if(vv < -250) vv = -250;

	background.hbridge.voltage = vv;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Get open loop modulation depth

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
float32 get_hbridge_voltage(void)
{
	return background.hbridge.voltage;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Check if holding

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
uint16_t is_hbridge_hold(void)
{
	return ( 	(hbridge_state.current == &hbridge_state_stop)||
				(hbridge_state.current == &hbridge_state_open_loop_hold)||
				(hbridge_state.current == &hbridge_state_DC_bus_comp_hold)	);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Check if running OL

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
uint16_t is_hbridge_open_loop(void)
{
	return (background.hbridge.mode == HBRIDGE_MODE_OPEN_LOOP);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Check if running DC bus compensated

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
uint16_t is_hbridge_dc_bus_comp(void)
{
	return (background.hbridge.mode == HBRIDGE_MODE_DC_BUS_COMP);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Check if no mode set

\author S.Parker
\par History:
\li	16/05/15 SP - initial creation
*/
uint16_t is_hbridge_none(void)
{
	return (background.hbridge.mode == HBRIDGE_MODE_NONE);
}

// end fsm_hbridge.c
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
