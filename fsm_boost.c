/**
\file
\brief Boost state machine functions

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
\li 12/06/15 SP - split boost and hbridge
*/

// compiler standard include files
#include <stdint.h>

// processor standard include files
#include "F2806x_Device.h"
#include "F2806x_GlobalPrototypes.h"

// local include files
#include "analogue.h"
#include "power_stage.h"
#include "fsm.h"
#include "cla.h"
#include "fault.h"

/*  =========================================================================
__Definitions()
============================================================================ */

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Local_Function_Prototypes()
============================================================================ */

/** @name State Machine Functions */
//@{
void boost_state_init(void);
void boost_state_stop(void);
void boost_state_open_loop_hold(void);
void boost_state_open_loop_ramp(void);
void boost_state_open_loop_run(void);
void boost_state_current_reg_hold(void);
void boost_state_current_reg_ramp(void);
void boost_state_current_reg_run(void);
void boost_state_bus_reg_hold(void);
void boost_state_bus_reg_ramp(void);
void boost_state_bus_reg_run(void);
void boost_state_fault(void);
//@}

/* =========================================================================
__Variables()
============================================================================ */

/// State machine pointer and variables
type_state boost_state = { &boost_state_init, (statePtr)0, 1, 0 };

/* =========================================================================
__Local_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialisation State
- Performs boost power stage initialisation

\author S.Parker
\par History:
\li	29/12/14 SP - initial creation
*/
void boost_state_init(void)
{
	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "Init  ";
		boost_state.first_time = 0;

		SWITCHING_DISABLE_BOOST();

		background.boost.init = 0;

		// Prevent accidental queuing transition to run state
		background.boost.enable = 0;
	}

	// Initialise interrupt variables
	background.boost.dutycycle = DEFAULT_DUTY_CYCLE;
	cpu2cla.boost.dutycycle = (float32)background.boost.dutycycle*0.01;

	/* State transitions */
	boost_state.current = (statePtr)&boost_state_stop;
	return;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Waits for mode setting

\author S.Parker
\par History:
\li	12/06/15 SP - initial creation
*/
void boost_state_stop(void)
{
	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "Stop  ";
		boost_state.first_time = 0;

		SWITCHING_DISABLE_BOOST();

		background.boost.init = 1;
	}

	// Zero out the interrupt variables
	cpu2cla.boost.dutycycle = DEFAULT_DUTY_CYCLE*0.01;

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		boost_state.current = (statePtr)&boost_state_fault;
		return;
	}

	if(background.boost.mode == BOOST_MODE_OPEN_LOOP)
	{
		// Enable boost only
		boost_state.current = (statePtr)&boost_state_open_loop_hold;
		return;
	}
	if(background.boost.mode == BOOST_MODE_CURRENT_REGULATION)
	{
		// Enable boost only
		boost_state.current = (statePtr)&boost_state_current_reg_hold;
		return;
	}
	if(background.boost.mode == BOOST_MODE_BUS_REGULATION)
	{
		// Enable boost only
		boost_state.current = (statePtr)&boost_state_bus_reg_hold;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Wait for flag to start

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void boost_state_open_loop_hold(void)
{
	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "OL Hold";
		boost_state.first_time = 0;

		SWITCHING_DISABLE_BOOST();
	}

	// Zero out the interrupt variables
	cpu2cla.boost.dutycycle = 1.00;

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		boost_state.current = (statePtr)&boost_state_fault;
		return;
	}

	// State transition for exit open loop
	if(background.boost.mode != BOOST_MODE_OPEN_LOOP)
	{
		boost_state.current = (statePtr)&boost_state_stop;
		return;
	}

	// State transition for enable
	if(background.boost.enable == 1)
	{
		cpu2cla.boost.dutycycle = 1.00;
		boost_state.current = (statePtr)&boost_state_open_loop_ramp;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Open Loop Ramp State - Boost
- Ramp modulation

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void boost_state_open_loop_ramp(void)
{
	static uint16_t ramp_done;

	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "OL Ramp";
		boost_state.first_time = 0;

		SWITCHING_ENABLE_BOOST();

		ramp_done = 0;
	}

	// Ramp interrupt commands by 1% per ms
	if(cpu2cla.boost.dutycycle > (float32)background.boost.dutycycle*0.01)
	{
		cpu2cla.boost.dutycycle -= 0.01;
	}
	else
	{
		cpu2cla.boost.dutycycle = (float32)background.boost.dutycycle*0.01;
		ramp_done |= 1;
	}

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		boost_state.current = (statePtr)&boost_state_fault;
		return;
	}

	// State transitions for disable
	if(background.boost.enable != 1)
	{
		boost_state.current = (statePtr)&boost_state_open_loop_hold;
		return;
	}

	// State transition for ramping completed
	if(ramp_done == 1)
	{
		boost_state.current = (statePtr)&boost_state_open_loop_run;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Open Loop Run State - HBridge only
- Everything is good, output is enabled and running

\author S.Parker
\par History:
\li	07/02/15 SP - initial creation
*/
void boost_state_open_loop_run(void)
{
	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "OL Run ";
		boost_state.first_time = 0;

		SWITCHING_ENABLE_BOOST();
	}

	// Feed through command value to CLA
	cpu2cla.boost.dutycycle = (float32)background.boost.dutycycle*0.01;

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		boost_state.current = (statePtr)&boost_state_fault;
		return;
	}

	// State transitions for disable
	if(background.boost.enable != 1)
	{
		boost_state.current = (statePtr)&boost_state_open_loop_hold;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Current regulation hold state

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void boost_state_current_reg_hold(void)
{
	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "CR Hold";
		boost_state.first_time = 0;

		SWITCHING_DISABLE_BOOST();
	}

	// Zero out the interrupt variables
	cpu2cla.boost.current = 0;

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		boost_state.current = (statePtr)&boost_state_fault;
		return;
	}

	// State transition for exit open loop
	if(background.boost.mode != BOOST_MODE_CURRENT_REGULATION)
	{
		boost_state.current = (statePtr)&boost_state_stop;
		return;
	}

	// State transition for enable
	if(background.boost.enable == 1)
	{
		cpu2cla.boost.current = get_adc_iboost();
		boost_state.current = (statePtr)&boost_state_current_reg_ramp;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Current regulation ramp state

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void boost_state_current_reg_ramp(void)
{
	static uint16_t ramp_done;

	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "CR Ramp";
		boost_state.first_time = 0;

		// Reset CLA variables
		Cla1ForceTask3andWait();
		SWITCHING_ENABLE_BOOST();

		ramp_done = 0;
	}

	// Ramp interrupt commands by 0.1A per ms
	if(cpu2cla.boost.current < (float32)background.boost.current*0.1)
	{
		cpu2cla.boost.current += 0.1;
	}
	else
	{
		cpu2cla.boost.current = (float32)background.boost.current*0.1;
		ramp_done |= 1;
	}

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		boost_state.current = (statePtr)&boost_state_fault;
		return;
	}

	// State transitions for disable
	if(background.boost.enable != 1)
	{
		boost_state.current = (statePtr)&boost_state_current_reg_hold;
		return;
	}

	// State transition for ramping completed
	if(ramp_done == 1)
	{
		boost_state.current = (statePtr)&boost_state_current_reg_run;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Current regulation run state

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void boost_state_current_reg_run(void)
{
	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "CR Run ";
		boost_state.first_time = 0;

		SWITCHING_ENABLE_BOOST();
	}

	// Feed through command value to CLA
	cpu2cla.boost.current = (float32)background.boost.current*0.1;

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		boost_state.current = (statePtr)&boost_state_fault;
		return;
	}

	// State transitions for disable
	if(background.boost.enable != 1)
	{
		boost_state.current = (statePtr)&boost_state_current_reg_hold;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
DC bus regulation hold state

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void boost_state_bus_reg_hold(void)
{
	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "BR Hold";
		boost_state.first_time = 0;

		SWITCHING_DISABLE_BOOST();
	}

	// Zero out the interrupt variables
	cpu2cla.boost.voltage = get_adc_vdc();

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		boost_state.current = (statePtr)&boost_state_fault;
		return;
	}

	// State transition for exit open loop
	if(background.boost.mode != BOOST_MODE_BUS_REGULATION)
	{
		boost_state.current = (statePtr)&boost_state_stop;
		return;
	}

	// State transition for enable
	if(background.boost.enable == 1)
	{
		cpu2cla.boost.voltage = get_adc_vdc();
		boost_state.current = (statePtr)&boost_state_bus_reg_ramp;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
DC bus regulation ramp state

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void boost_state_bus_reg_ramp(void)
{
	static uint16_t ramp_done;

	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "BR Ramp";
		boost_state.first_time = 0;

		// Reset CLA variables
		Cla1ForceTask3andWait();
		SWITCHING_ENABLE_BOOST();

		ramp_done = 0;
	}

	// Ramp interrupt commands by 1V per ms
	if(cpu2cla.boost.voltage < (float32)background.boost.voltage)
	{
		cpu2cla.boost.voltage += 1;
	}
	else
	{
		cpu2cla.boost.voltage = (float32)background.boost.voltage;
		ramp_done |= 1;
	}

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		boost_state.current = (statePtr)&boost_state_fault;
		return;
	}

	// State transitions for disable
	if(background.boost.enable != 1)
	{
		boost_state.current = (statePtr)&boost_state_bus_reg_hold;
		return;
	}

	// State transition for ramping completed
	if(ramp_done == 1)
	{
		boost_state.current = (statePtr)&boost_state_bus_reg_run;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
DC bus regulation run state

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void boost_state_bus_reg_run(void)
{
	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "BR Run ";
		boost_state.first_time = 0;

		SWITCHING_ENABLE_BOOST();
	}

	// Feed through command value to CLA
	cpu2cla.boost.voltage = (float32)background.boost.voltage;

	/* State transitions */

	// State transition for faults
	if(detected_faults != 0)
	{
		boost_state.current = (statePtr)&boost_state_fault;
		return;
	}

	// State transitions for disable
	if(background.boost.enable != 1)
	{
		boost_state.current = (statePtr)&boost_state_bus_reg_hold;
		return;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Fault State
- Performs fault handling (as yet undefined)

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void boost_state_fault(void)
{
	if(boost_state.first_time == 1)
	{
		boost_state.state_name = "Fault  ";
		boost_state.first_time = 0;

		SWITCHING_DISABLE_BOOST();

		// Prevent queuing transistion to run state
		background.boost.enable = 0;
	}

	/* State transitions */

	// No faults remaining
	if(detected_faults == 0)
	{
		boost_state.current = (statePtr)&boost_state_stop;
		return;
	}
}

/* =========================================================================
__Exported_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Runs the current boost state function

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void run_boost_fsm(void)
{
	// Flag state transition
	boost_state.first_time = (boost_state.prev != boost_state.current);
	boost_state.prev = boost_state.current;

	// Call current state
	CALL_STATE(boost_state);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Set boost operating mode

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void set_boost_mode(uint16_t mode)
{
	if(is_boost_hold()&&is_hbridge_hold())
	{
		background.boost.mode = mode;
		map_cla_task1();
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Get boost operating mode

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
int16_t get_boost_mode(void)
{
	return background.boost.mode;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Enable boost converter

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void enable_boost(void)
{
	if( (boost_state.current == &boost_state_open_loop_hold) ||
		(boost_state.current == &boost_state_current_reg_hold)||
		(boost_state.current == &boost_state_bus_reg_hold) )
	{
		background.boost.enable = 1;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Disable boost converter

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void disable_boost(void)
{
	background.boost.enable = 0;
	SWITCHING_DISABLE_BOOST();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns name of current state

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
char* get_boost_state_name(void)
{
	return boost_state.state_name;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Set duty cycle (open loop)

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void set_boost_dutycycle(int16_t dc)
{
	if(dc > 100) dc = 100;
	if(dc < 0) dc = 0;

	background.boost.dutycycle = dc;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Get duty cycle (open loop)

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
float32 get_boost_dutycycle(void)
{
	return background.boost.dutycycle;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Set current target (current regulation0

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void set_boost_current(int16_t aa)
{
	if(aa > 80) aa = 80;
	if(aa < 0) aa = 0;

	background.boost.current = aa;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Get current target (current regulation)

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
float32 get_boost_current(void)
{
	return background.boost.current;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Set DC bus target (bus regulation)

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
void set_boost_voltage(int16_t vv)
{
	if(vv > 500) vv = 500;
	if(vv < 0) vv = 0;

	background.boost.voltage = vv;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Get DC bus target (bus regulation)

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
float32 get_boost_voltage(void)
{
	return background.boost.voltage;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Check if boost state hold

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
uint16_t is_boost_hold(void)
{
	return (	(boost_state.current == &boost_state_stop)||
				(boost_state.current == &boost_state_open_loop_hold)||
				(boost_state.current == &boost_state_current_reg_hold)||
				(boost_state.current == &boost_state_bus_reg_hold)	);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Check if boost state open loop

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
uint16_t is_boost_openloop(void)
{
	return (background.boost.mode == BOOST_MODE_OPEN_LOOP);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Check if boost state current regulation

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
uint16_t is_boost_currentreg(void)
{
	return (background.boost.mode == BOOST_MODE_CURRENT_REGULATION);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Check if boost state bus regulation

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
uint16_t is_boost_busreg(void)
{
	return (background.boost.mode == BOOST_MODE_BUS_REGULATION);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Check if boost state not set

\author S.Parker
\par History:
\li	05/04/15 SP - initial creation
*/
uint16_t is_boost_none(void)
{
	return (background.boost.mode == BOOST_MODE_NONE);
}

// end fsm_boost.c
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
