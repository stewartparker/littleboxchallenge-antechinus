/**
\file
\brief Lifesupport initialization for F28069 for LBC "Antechinus" Prototype Rev2

Derived from From TI supplied DeadBandPWM_DevInit_F2806x.c.

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
\li 16/05/15 SP - port to Rev 2
*/

// compiler standard include files
#include <stdint.h>

// processor standard include files
#include "F2806x_Device.h"
#include "F2806x_GlobalPrototypes.h"

// local include files
#include "power_stage.h"

/*  =========================================================================
__Definitions()
============================================================================ */

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Variables()
============================================================================ */

/* =========================================================================
__Function_Prototypes()
============================================================================ */

/// Initialise the operation of the flash memory
#pragma CODE_SECTION(InitFlash, "ramfuncs");
void InitFlash(void);

/// Basic initialisaiton of the microcontroller
void DeviceInit(void);

/// Enable interrupts in PIE and CPU
void EnableInterrupts(void);

/// Copy memory from one location to another
void MemCopy(uint16_t *SourceAddr, uint16_t* SourceEndAddr, uint16_t* DestAddr);

/// Initialise PIE control
void PieCntlInit(void);

/// Initialise PIE table
void PieVectTableInit(void);

/// Disable WatchDog
void WDogDisable(void);

/// Setup PLL for clocks
void PLLset(uint16_t val, uint16_t divsel);

/// Interrupt for illegal operation
interrupt void isr_illegal(void);

/* =========================================================================
__Exported_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Configure device for target application

\author S.Parker
\par History:
\li	25/07/13 SP - Initial Creation from TI code base
\li 16/05/15 SP - GPIO for Rev2
*/
void DeviceInit(void)
{
	WDogDisable(); 	// Disable the watchdog initially
	DINT;			// Global Disable all Interrupts
	IER = 0x0000;	// Disable CPU interrupts
	IFR = 0x0000;	// Clear all CPU interrupt flags

	// Switch to external crystal oscillator (on X1, X2) and turn off all other clock
	// sources to minimize power consumption
	EALLOW;
	SysCtrlRegs.CLKCTL.bit.XTALOSCOFF = 0;     // Turn on XTALOSC
	SysCtrlRegs.CLKCTL.bit.XCLKINOFF = 1;      // Turn off XCLKIN
	SysCtrlRegs.CLKCTL.bit.OSCCLKSRC2SEL = 0;  // Switch to external clock
	SysCtrlRegs.CLKCTL.bit.OSCCLKSRCSEL = 1;   // Switch from INTOSC1 to INTOSC2/ext clk
	SysCtrlRegs.CLKCTL.bit.WDCLKSRCSEL = 0;    // Clock Watchdog from INTOSC1 always
	SysCtrlRegs.CLKCTL.bit.INTOSC2OFF = 1;     // Turn off INTOSC2
	SysCtrlRegs.CLKCTL.bit.INTOSC1OFF = 0;     // Leave INTOSC1 on
	EDIS;

	// Set 90MHz based on 20MHz external oscillator
	// div = 18, divsel = 0
	// (OSCCLK * 18) / 4
	PLLset( 0x12, 0 );

	// Initialise interrupt controller and Vector Table
	// to defaults for now. Application ISR mapping done later.
	PieCntlInit();		
	PieVectTableInit();

	EALLOW; // below registers are "protected", allow access.

	// LOW SPEED CLOCKS prescale register settings
	SysCtrlRegs.LOSPCP.all = 0x0002;		// Sysclk / 4 (25 MHz)
	SysCtrlRegs.XCLK.bit.XCLKOUTDIV=2;

	// Peripheral clocks are enabled in their specific peripheral initialisation function

	/* Basic configuration for Rev2 */
	// GPA
	// GPIO9  = oFAN2_CTRL
	// GPIO14 = oFAN1_CTRL
	// GPIO22 = inSW_SHDN
	// GPIO24 = inREM_SHDN
	GpioCtrlRegs.GPAMUX1.all = 0x00000000; // 0000 0000 0000 0000 0000 0000 0000 0000
	GpioCtrlRegs.GPAMUX2.all = 0x00000000; // 0000 0000 0000 0000 0000 0000 0000 0000
	GpioCtrlRegs.GPADIR.all = 0x00004200; // 0000 0000 0000 0000 0100 0010 0000 0000
	GpioDataRegs.GPASET.all = 0x01400000; // 0000 0000 0000 0000 0000 0000 0000 0000
	// GPB
	// GPIO32 = oLED1
	// GPIO33 = oLED2
	GpioCtrlRegs.GPBMUX1.all = 0x00000000; // 0000 0000 0000 0000 0000 0000 0000 0000
	GpioCtrlRegs.GPBMUX2.all = 0x00000000; // 0000 0000 0000 0000 0000 0000 0000 0000
	GpioCtrlRegs.GPBDIR.all = 0x00000003; // 0000 0000 0000 0000 0000 0000 0000 0011
	GpioDataRegs.GPBSET.all = 0x00000003; // 0000 0000 0000 0000 0000 0000 0000 0011

	EDIS;	// Disable register access
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
This function initializes the Flash Control registers

					CAUTION
This function MUST be executed out of RAM. Executing it
out of OTP/Flash will yield unpredictable results

\author S.Paker
\par History:
\li	03/10/14 SP - Initial Creation from TI code base
*/
void InitFlash(void)
{
   EALLOW;
   //Enable Flash Pipeline mode to improve performance
   //of code executed from Flash.
   FlashRegs.FOPT.bit.ENPIPE = 1;

   //                CAUTION
   //Minimum waitstates required for the flash operating
   //at a given CPU rate must be characterized by TI.
   //Refer to the datasheet for the latest information.

   //Set the Paged Waitstate for the Flash
   FlashRegs.FBANKWAIT.bit.PAGEWAIT = 3;

   //Set the Random Waitstate for the Flash
   FlashRegs.FBANKWAIT.bit.RANDWAIT = 3;

   //Set the Waitstate for the OTP
   FlashRegs.FOTPWAIT.bit.OTPWAIT = 5;

   //                CAUTION
   //ONLY THE DEFAULT VALUE FOR THESE 2 REGISTERS SHOULD BE USED
   FlashRegs.FSTDBYWAIT.bit.STDBYWAIT = 0x01FF;
   FlashRegs.FACTIVEWAIT.bit.ACTIVEWAIT = 0x01FF;
   EDIS;

   //Force a pipeline flush to ensure that the write to
   //the last register configured occurs before returning.
   asm(" RPT #7 || NOP");
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Enables interrupts in the PIE and CPU

\author S.Paker
\par History:
\li	03/10/14 SP - Initial Creation from TI code base
*/
void EnableInterrupts(void)
{

    // Enable the PIE
    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;

	// Enables PIE to drive a pulse into the CPU
	PieCtrlRegs.PIEACK.all = 0xFFFF;

	// Enable Interrupts at the CPU level
    EINT;

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
This function will copy the specified memory contents from
one location to another. 

uint16_t *SourceAddr        Pointer to the first word to be moved
                         SourceAddr < SourceEndAddr
uint16_t* SourceEndAddr     Pointer to the last word to be moved
uint16_t* DestAddr          Pointer to the first destination word
No checks are made for invalid memory locations or that the
end address is > then the first start address.

\author S.Paker
\par History:
\li	03/10/14 SP - Initial Creation from TI code base
*/
void MemCopy(uint16_t *SourceAddr, uint16_t* SourceEndAddr, uint16_t* DestAddr)
{
    while(SourceAddr < SourceEndAddr)
    { 
       *DestAddr++ = *SourceAddr++;
    }
    return;
}

/* =========================================================================
__Local_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Disable watchdog timer

\author S.Paker
\par History:
\li	03/10/14 SP - Initial Creation from TI code base
*/
void WDogDisable(void)
{
	EALLOW;
	SysCtrlRegs.WDCR= 0x0068;
	EDIS;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialises the clock PLL

\author S.Paker
\par History:
\li	03/10/14 SP - Initial Creation from TI code base
*/
void PLLset(Uint16 val, Uint16 divsel)
{

	// Make sure the PLL is not running in limp mode
	if (SysCtrlRegs.PLLSTS.bit.MCLKSTS != 0)
	{
		EALLOW;
		// OSCCLKSRC1 failure detected. PLL running in limp mode.
		// Re-enable missing clock logic.
		SysCtrlRegs.PLLSTS.bit.MCLKCLR = 1;
		EDIS;
		// Replace this line with a call to an appropriate
		// SystemShutdown(); function.
		//asm("        ESTOP0");     // Uncomment for debugging purposes
	}

	// DIVSEL MUST be 0 before PLLCR can be changed from
	// 0x0000. It is set to 0 by an external reset XRSn
	// This puts us in 1/4
	if (SysCtrlRegs.PLLSTS.bit.DIVSEL != 0)
	{
		EALLOW;
		SysCtrlRegs.PLLSTS.bit.DIVSEL = 0;
		EDIS;
	}

	// Change the PLLCR
	if (SysCtrlRegs.PLLCR.bit.DIV != val)
	{

		EALLOW;
		// Before setting PLLCR turn off missing clock detect logic
		SysCtrlRegs.PLLSTS.bit.MCLKOFF = 1;
		SysCtrlRegs.PLLCR.bit.DIV = val;
		EDIS;

		// Optional: Wait for PLL to lock.
		// During this time the CPU will switch to OSCCLK/2 until
		// the PLL is stable.  Once the PLL is stable the CPU will
		// switch to the new PLL value.
		//
		// This time-to-lock is monitored by a PLL lock counter.
		//
		// Code is not required to sit and wait for the PLL to lock.
		// However, if the code does anything that is timing critical,
		// and requires the correct clock be locked, then it is best to
		// wait until this switching has completed.

		// Wait for the PLL lock bit to be set.
		// The watchdog should be disabled before this loop, or fed within
		// the loop via ServiceDog().

		while(SysCtrlRegs.PLLSTS.bit.PLLLOCKS != 1)
		{
		}

		EALLOW;
		SysCtrlRegs.PLLSTS.bit.MCLKOFF = 0;
		EDIS;
	}

	EALLOW;
	SysCtrlRegs.PLLSTS.bit.DIVSEL = divsel;
	EDIS;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialises the PIE control registers to a known state

\author S.Paker
\par History:
\li	03/10/14 SP - Initial Creation from TI code base
*/
void PieCntlInit(void)
{
    // Disable Interrupts at the CPU level:
    DINT;

    // Disable the PIE
    PieCtrlRegs.PIECTRL.bit.ENPIE = 0;

	// Clear all PIEIER registers:
	PieCtrlRegs.PIEIER1.all = 0;
	PieCtrlRegs.PIEIER2.all = 0;
	PieCtrlRegs.PIEIER3.all = 0;	
	PieCtrlRegs.PIEIER4.all = 0;
	PieCtrlRegs.PIEIER5.all = 0;
	PieCtrlRegs.PIEIER6.all = 0;
	PieCtrlRegs.PIEIER7.all = 0;
	PieCtrlRegs.PIEIER8.all = 0;
	PieCtrlRegs.PIEIER9.all = 0;
	PieCtrlRegs.PIEIER10.all = 0;
	PieCtrlRegs.PIEIER11.all = 0;
	PieCtrlRegs.PIEIER12.all = 0;

	// Clear all PIEIFR registers:
	PieCtrlRegs.PIEIFR1.all = 0;
	PieCtrlRegs.PIEIFR2.all = 0;
	PieCtrlRegs.PIEIFR3.all = 0;	
	PieCtrlRegs.PIEIFR4.all = 0;
	PieCtrlRegs.PIEIFR5.all = 0;
	PieCtrlRegs.PIEIFR6.all = 0;
	PieCtrlRegs.PIEIFR7.all = 0;
	PieCtrlRegs.PIEIFR8.all = 0;
	PieCtrlRegs.PIEIFR9.all = 0;
	PieCtrlRegs.PIEIFR10.all = 0;
	PieCtrlRegs.PIEIFR11.all = 0;
	PieCtrlRegs.PIEIFR12.all = 0;
}	

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialises tbe PIE vector table for illegal operations

\author S.Paker
\par History:
\li	03/10/14 SP - Initial Creation from TI code base
*/
void PieVectTableInit(void)
{
	uint16_t i;
   	PINT *Dest = (PINT*)&PieVectTable.TINT1;

   	EALLOW;
   	for(i=0; i < 115; i++) 
    *Dest++ = &isr_illegal;
   	EDIS;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Illegal operation interrupt - disable converter operation and reset

\author S.Paker
\par History:
\li	03/10/14 SP - Initial Creation from TI code base
*/
interrupt void isr_illegal(void)
{
	// Turn off switches
	SWITCHING_DISABLE();

	EALLOW;
	// Trigger a watchdog event
	SysCtrlRegs.WDCR = 0;
	EDIS;
}

// end init_F28069.c
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
