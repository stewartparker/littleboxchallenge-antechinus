/**
\file
\brief CLA definitions and functions

\author S.Parker
\par History:
\li	18/10/14 SP - initial creation
*/
#ifndef CLA_H_
#define CLA_H_

/* =========================================================================
__Definitions()
============================================================================ */

/// Macro to force task 3 (reset variables)
#define Cla1ForceTask3andWait()__asm("  IACK  #0x0004");             \
                               __asm("  RPT #3 || NOP");             \
                                while(Cla1Regs.MIRUN.bit.INT3 == 1);

/* =========================================================================
__Typedefs()
============================================================================ */

/// Holds analogue results and scaling factors
typedef struct
{
	float32
		real,
		scale,
		offset;
} type_real_cla_adc;

/// Holds values for modulator calculations
typedef struct
{
	float32
		pu,
		real,
		scale,
		offset,
		max,
		min;
} type_cmpr;

/// Sine and cosine trig values
typedef struct
{
	float32
		sin,
		cos;
} type_trig;

/// PI controller variables
typedef struct
{
	float32
		error,
		integral,
		Kp,
		Ki,
		output;
} type_pi_control;

/// Feedforward variables
typedef struct
{
	float32
		error,
		alpha,
		beta,
		frequency,
		K,
		direct,
		quadrature,
		scale;
} type_ff_control;

/** @name CPU to CLA structures */
//@{
/// H-bridge commands from CPU
typedef struct
{
	float32
		moddepth,
		voltage;
} type_cpu2cla_hbridge;

/// Boost commands from CPU
typedef struct
{
	float32
		dutycycle,
		current,
		voltage;
} type_cpu2cla_boost;

/// Analogue values from CPU
typedef struct
{
	type_real_cla_adc
		vzero,
		vdc_average;
	int16
		average_flag;
} type_cpu2cla_adc;

/// Generic structure for all structures from the CPU
typedef struct
{
	type_cpu2cla_adc
		adc;
	type_cpu2cla_hbridge
		hbridge;
	type_cpu2cla_boost
		boost;
} type_cpu2cla;
//@}

/** @name CLA to CPU structures */
//@{
/// Analogue values to the CPU
typedef struct
{
	type_real_cla_adc
		vdc,
		iboost,
		vin,
		iac;
} type_cla2cpu_adc;

/// Phasor values to the CPU
typedef struct
{
	Uint32
		current,
		step;
} type_cla2cpu_phase;

/// Generic structure for all structures to the CPU
typedef struct
{
	type_cla2cpu_adc
		adc;
	type_cla2cpu_phase
		phase;
} type_cla2cpu;
//@}

/* =========================================================================
__Exported_Variables()
============================================================================ */

/** @name Variables exchanged between CPU and CLA */
//@{
extern type_cpu2cla cpu2cla;
extern type_cla2cpu cla2cpu;
//@}

/** @name Variables local to the CLA */
//@{
extern float32 CLA_sin_table[];
extern float32 *CLA_cos_table;
//@}

/* =========================================================================
__Exported_Functions()
============================================================================ */

/// Initialise the CLA
void InitCLA(void);

/// Map correct control function to CLA task 1
void map_cla_task1(void);

/** @name CLA Task 1 */
//@{
/// Boost OL, Hbridge OL
#pragma CODE_SECTION(ClaTask1_Bol_Hol, "clafuncs");
__interrupt void ClaTask1_Bol_Hol ( void );
/// Boost CR, Hbridge OL
#pragma CODE_SECTION(ClaTask1_Bcr_Hol, "clafuncs");
__interrupt void ClaTask1_Bcr_Hol ( void );
/// Boost BR, Hbridge OL
#pragma CODE_SECTION(ClaTask1_Bbr_Hol, "clafuncs");
__interrupt void ClaTask1_Bbr_Hol ( void );
/// Boost BR, Hbridge OL
#pragma CODE_SECTION(ClaTask1_Bol_Hdc, "clafuncs");
__interrupt void ClaTask1_Bol_Hdc ( void );
/// Boost OL, Hbridge DC
#pragma CODE_SECTION(ClaTask1_Bcr_Hdc, "clafuncs");
__interrupt void ClaTask1_Bcr_Hdc ( void );
/// Boost CR, Hbridge DC
#pragma CODE_SECTION(ClaTask1_Bbr_Hdc, "clafuncs");
__interrupt void ClaTask1_Bbr_Hdc ( void );
/// Boost BR, Hbridge OL
#pragma CODE_SECTION(ClaTask3_InitCLA, "clafuncs");
__interrupt void ClaTask3_InitCLA ( void );
//@}

#endif /* CLA_H_ */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
