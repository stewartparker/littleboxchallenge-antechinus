/**
\file
\brief  Header for ADC functions and variables

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
#ifndef ANALOGUE_H_
#define ANALOGUE_H_

/* =========================================================================
__Definitions()
============================================================================ */

/// ADC calibration function location
#define Device_cal (void(*)(void))0x3D7C80

/// ADC temperature scale value locations
#define getTempSlope() (*(int (*)(void))0x3D7E82)() //Slope of temperature sensor (deg. C / ADC code, fixed pt Q15 format)
#define getTempOffset() (*(int (*)(void))0x3D7E85)() //ADC code corresponding to temperature sensor output at 0-degreesC

/* =========================================================================
__Typedefs()
============================================================================ */

/** @name ADC result storage structures */
//@{
/// Contains only the scaled result
typedef struct
{
	float32
		real;
} type_real_adc;

/// Contains the scaled result and scale factor and offset
typedef struct
{
	float32
		real,
		scale,
		offset;
} type_real_scale_adc;

/// Contains variables for averaging
typedef struct
{
	float32
		sum,
		real;
	int16
		count,
		flag;
} type_adc_average;

/// Overall structure for analogue values
typedef struct
{
	type_real_adc
		vin,
		iac,
		iboost,
		vdc,
		vzero;
	type_real_scale_adc
		vout,
		temp;
	type_adc_average
		vdc_average;
} type_adc;
//@}

/* =========================================================================
__Exported_Variables()
============================================================================ */

/// Globally accessible analogue results
extern type_adc adc;

/* =========================================================================
__Exported_Functions()
============================================================================ */

/// Initialise ADC sequence
void InitADC(void);

/// Initialise protection comparators
void InitComparators(void);

/** @name Return analogue measured value */
//@{
/// Zero voltage measurement in real units
float32 get_adc_vzero(void);
/// Input voltage
float32 get_adc_vin(void);
/// Input current
float32 get_adc_iac(void);
/// Output voltage
float32 get_adc_vout(void);
/// DC bus voltage
float32 get_adc_vdc(void);
/// Averaged DC bus voltage
float32 get_adc_vdc_average(void);
/// Input current
float32 get_adc_iboost(void);
/// Temperature
float32 get_adc_temp(void);
//@}


#endif /* ANALOGUE_H_ */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

