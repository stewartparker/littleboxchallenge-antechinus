/**
\file
\brief  State machine functions and macros

\author S.Parker
\par History:
\li	12/06/15 SP - initial creation
*/
#ifndef FSM_H_
#define FSM_H_

/* =========================================================================
__Definitions()
============================================================================ */

/// Square root 2
#define _SQRT2 1.41421356

/// Start CPU 1 timer for _tt_ counts
#define START_FSM_TIMER(_tt_) { CpuTimer1Regs.PRD.half.LSW = _tt_&0x0000FFFF; \
								CpuTimer1Regs.PRD.half.MSW = (_tt_&0xFFFF0000)>>16; \
								CpuTimer1Regs.TCR.bit.TRB = 1; \
								CpuTimer1Regs.TCR.bit.TIF = 1; \
								CpuTimer1Regs.TCR.bit.TSS = 0; }

/// Check CPU timer 1 for completion
#define CHECK_FSM_TIMER() CpuTimer1Regs.TCR.bit.TIF

/// Call current state function
#define CALL_STATE(_s_) (*(_s_.current))()

/** @name System Modes of Operation */
//@{
#define SYS_MODE_NONE	0
#define SYS_MODE_DEV	1
#define SYS_MODE_PROD	2
//@}

/** @name Boost Modes of operation */
//@{
#define BOOST_MODE_NONE 0
#define BOOST_MODE_OPEN_LOOP 1
#define BOOST_MODE_CURRENT_REGULATION 2
#define BOOST_MODE_BUS_REGULATION 3
//@}

/** @name HBridge Modes of operation */
//@{
#define HBRIDGE_MODE_NONE 0
#define HBRIDGE_MODE_OPEN_LOOP 1
#define HBRIDGE_MODE_DC_BUS_COMP 2
//@}

/* =========================================================================
__Typedefs()
============================================================================ */

/// State function pointer type
typedef void (* statePtr)(void);

/// State machine structure for current state details
typedef struct
{
	statePtr current;
	statePtr prev;
	uint16_t first_time;
	char* state_name;
} type_state;

/// Hbridge background settings
typedef struct
{
	int16_t
		init,
		mode,
		enable;
	int16_t
		moddepth,
		voltage;
} type_hbridge;

/// Boost background settings
typedef struct
{
	int16_t
		init,
		mode,
		enable;
	int16_t
		dutycycle,
		current,
		voltage;
} type_boost;

/// Overall background settging structure
typedef struct
{
	uint16_t
		system_mode;
	type_hbridge
		hbridge;
	type_boost
		boost;
} type_background;

/* =========================================================================
__Exported_Variables()
============================================================================ */

/// Background settings for each state machine
extern type_background background;

/* =========================================================================
__Exported_Functions()
============================================================================ */

/** @name  Boost State Machine */
//@{
/// Run boost state machine
void run_boost_fsm(void);

/// Get boost state name
char* get_boost_state_name(void);

/// Set and get boost converter mode
void set_boost_mode(uint16_t mode);
int16_t get_boost_mode(void);

/// Check boost converter mode
uint16_t is_boost_hold(void);
uint16_t is_boost_openloop(void);
uint16_t is_boost_currentreg(void);
uint16_t is_boost_busreg(void);
uint16_t is_boost_none(void);

/// Enable and disable boost converter
void enable_boost(void);
void disable_boost(void);

/// Set and get open loop duty cycle
void set_boost_dutycycle(int16_t);
float32 get_boost_dutycycle(void);

/// Set and get current target
void set_boost_current(int16_t aa);
float32 get_boost_current(void);

/// Set and get bus voltage target
void set_boost_voltage(int16_t vv);
float32 get_boost_voltage(void);
//@}

/** @name HBridge State Machine */
//@{
/// Run Hbridge state machine
void run_hbridge_fsm(void);

/// Get Hbrudge state name
char* get_hbridge_state_name(void);

/// Set and get Hbridge converter mode
void set_hbridge_mode(uint16_t mode);
int16_t get_hbridge_mode(void);

/// Check Hbridge converter mode
uint16_t is_hbridge_hold(void);
uint16_t is_hbridge_open_loop(void);
uint16_t is_hbridge_dc_bus_comp(void);
uint16_t is_hbridge_none(void);

/// Enable and disable hbridge
void enable_hbridge(void);
void disable_hbridge(void);

/// Set and get output commands
void set_hbridge_moddepth(int16_t md);
float32 get_hbridge_moddepth(void);

/// Set and get target voltage
void set_hbridge_voltage(int16_t vv);
float32 get_hbridge_voltage(void);
//@}

/** @name System State Machine */
//@{
void run_system_fsm(void);
char* get_system_state_name(void);
uint16_t get_system_mode(void);
//@}

#endif /* FSM_H_ */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
