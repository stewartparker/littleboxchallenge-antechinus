/**
\file
\brief Power stage definitions

Macros and constants for control of the power stages

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
#ifndef POWERSTAGE_H_
#define POWERSTAGE_H_

/* =========================================================================
__Definitions()
============================================================================ */

/** @name Frequency and timing control */
//@{
/// Triangular carrier
#define F_CARRIER_BOOST 100e3
#define BOOST_PERIOD_2 ((Uint16)(90e6/F_CARRIER_BOOST/4.0)) // Although not used, ensures frequency is in sync with Hbridge
#define BOOST_PERIOD (BOOST_PERIOD_2<<1)
#define F_CALC_BOOST (F_CARRIER_BOOST) // Individual boost calculation and update rate

/// Triangular carrier
#define F_CARRIER_HBRIDGE 100e3
#define HBRIDGE_PERIOD_2 (Uint16)(90e6/F_CARRIER_HBRIDGE/4.0)
#define HBRIDGE_PERIOD (HBRIDGE_PERIOD_2<<1)
#define F_CALC_HBRIDGE (Uint32)(F_CARRIER_HBRIDGE) // HBridge calculation and update rate

/// Fundamental frequency
#define F_FUNDAMENTAL 60 // Per LBC specification
#define PHASE_STEP_SCALE (4294967296.0/(float32)F_CALC_HBRIDGE) // counts per interrupt per Hz
#define PHASE_STEP (Uint32)((float32)PHASE_STEP_SCALE*(float32)F_FUNDAMENTAL+0.5) // step size per interrupt
#define PHASE_180_DEG 	(Uint32)2147483648
//@}

/** @name Duty cycle limitations */
//@{
#define DEFAULT_MOD_DEPTH (int16)(0) //out of 100
#define MAX_MOD_DEPTH 0.97 //out of 1.00
#define MIN_MOD_DEPTH -MAX_MOD_DEPTH //out of 1.00
#define DEFAULT_DUTY_CYCLE (int16)(100) //out of 100
#define MAX_DUTY_CYCLE 1.0 //out of 1.00
#define MIN_DUTY_CYCLE 0.0 //out of 1.00
//@}

/** @name Macros for power stage enable / disable */
//@{
#define START_EPWM_TBCLK() { EALLOW; \
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1; \
	EDIS; }

/// Enable ePWM 1AB, 2AB, 4AB (set as epwm)
//#define SWITCHING_ENABLE() { EALLOW; \
//	GpioCtrlRegs.GPAMUX1.all |= 0x00005055; \
//	EDIS; }

/// Disable ePWM 1AB, 2AB, 4AB (set as output driving 0 except 2B)
#define SWITCHING_DISABLE() { EALLOW; \
	GpioDataRegs.GPACLEAR.all = 0x00CF; \
	GpioCtrlRegs.GPAMUX1.all &= ~(0x00005055); \
	EDIS; }

/// Enable ePWM 1AB, 2AB (set as epwm)
#define SWITCHING_ENABLE_HBRIDGE() { EALLOW; \
	GpioCtrlRegs.GPAMUX1.all |= 0x00000055; \
	EDIS; }

/// Disable ePWM 1AB, 2AB (set as output driving 0)
#define SWITCHING_DISABLE_HBRIDGE() { EALLOW; \
	GpioDataRegs.GPACLEAR.all = 0x000F; \
	GpioCtrlRegs.GPAMUX1.all &= ~(0x00000055); \
	EDIS; }

/// Enable ePWM 4AB (set as epwm)
#define SWITCHING_ENABLE_BOOST() { EALLOW; \
	GpioCtrlRegs.GPAMUX1.all |= 0x00005000; \
	EDIS; }

/// Disable ePWM 4AB (set as output driving 0)
#define SWITCHING_DISABLE_BOOST() { EALLOW; \
	GpioDataRegs.GPACLEAR.all = 0x00C0; \
	GpioCtrlRegs.GPAMUX1.all &= ~(0x00005000); \
	EDIS; }
//@}

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Exported_Variables()
============================================================================ */

/* =========================================================================
__Exported_Functions()
============================================================================ */

/// Initialise boost ePWMs
void InitBoost(void);

/// Initialise hbridge ePWMs
void InitHBridge(void);

#endif /* POWERSTAGE_H_ */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
