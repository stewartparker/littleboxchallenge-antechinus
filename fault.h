/**
\file
\brief Fault management definitions

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
#ifndef FAULT_H_
#define FAULT_H_

/* =========================================================================
__Definitions()
============================================================================ */

/** @name Faults with bitmasks */
//@{
#define FAULT_IBOOST_OC	0x0001
#define FAULT_IAC_OC	0x0002
#define FAULT_IBOOST_OC_SOFT	0x0004
#define FAULT_IAC_OC_SOFT	0x0008
#define FAULT_VDC_OV_SOFT	0x0010
#define FAULT_UNDEF6	0x0020
#define FAULT_UNDEF7	0x0040
#define FAULT_UNDEF8	0x0080
#define FAULT_UNDEF9	0x0100
#define FAULT_UNDEF10	0x0200
#define FAULT_UNDEF11	0x0400
#define FAULT_UNDEF12	0x0800
#define FAULT_UNDEF13	0x1000
#define FAULT_UNDEF14	0x2000
#define FAULT_UNKNOWN	0x4000
#define FAULT_WD_RESET	0x8000
//@}

/// Set fault bit macro - for interrupt use
#define FAULT_SET(_sfault_)	{ detected_faults |= _sfault_; }

/* =========================================================================
__Typedefs()
============================================================================ */

/// Currently present faults
extern volatile uint16_t detected_faults;

/* =========================================================================
__Exported_Variables()
============================================================================ */

/* =========================================================================
__Exported_Functions()
============================================================================ */

/// Set fault bit
void fault_set(uint16_t sfault);

/// Clear fault bit
void fault_clear(uint16_t cfault);

/// Clear faults
void fault_clear_all(void);

/// Return set faults
uint16_t fault_get(void);

#endif /* FAULT_H_ */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
