/**
\file
\brief Fault management

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/

// compiler standard include files
#include <stdint.h>

// processor standard include files
#include "F2806x_Device.h"
#include "F2806x_GlobalPrototypes.h"

// local include files
#include "fault.h"

/*  =========================================================================
__Definitions()
============================================================================ */

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Variables()
============================================================================ */

/// Current faults present in the system or not yet cleared
volatile uint16_t detected_faults = 0;

/* =========================================================================
__Exported_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Set fault bits

History:	04/10/14	SGP - Creation
*/
void fault_set(uint16_t sfault)
{
	detected_faults |= sfault;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Clear fault bits

History:	04/10/14	SGP - Creation
*/
void fault_clear(uint16_t cfault)
{
	detected_faults &= ~cfault;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Clear ePWM trip faults

History:	04/10/14	SGP - Creation
*/
void fault_clear_all(void)
{
	EALLOW;
	// Attempt to clear TZINT, OST and DCAEVT1 on ePWM1, ePWM2 and ePWM4
	// They will reset themselves if fault still present?
	EPwm1Regs.TZCLR.all = 0x002D; // Seemed to work better when clearing all 4 flags at once
	EPwm2Regs.TZCLR.all = 0x002D; // Seemed to work better when clearing all 4 flags at once
	EPwm4Regs.TZCLR.all = 0x000D; // Seemed to work better when clearing all 3 flags at once
	EDIS;

	detected_faults = 0;

	// Record IAC overcurrent if still present
	if( (EPwm1Regs.TZFLG.bit.DCAEVT1)||(EPwm1Regs.TZFLG.bit.DCBEVT1) )
	{
		FAULT_SET(FAULT_IAC_OC);
	}
	// Record IBOOST overcurrent if still present
	if(EPwm4Regs.TZFLG.bit.DCAEVT1)
	{
		FAULT_SET(FAULT_IBOOST_OC);
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Return fault bits

History:	04/10/14	SGP - Creation
*/
uint16_t fault_get(void)
{
	return detected_faults;
}

// end fault.c
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
