/**
\file
\brief  Serial interface header

Mostly functions to put values onto the serial display

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
\li 30/12/14 LB - added putfXX_com functions
*/
#ifndef _SERIAL_COM_H_
#define _SERIAL_COM_H_

/* =========================================================================
__Definitions()
============================================================================ */

/// Macro to put a new line
#define PUTNEWLINE_COM() puts_com("\r\n")

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Exported_Variables()
============================================================================ */

/* =========================================================================
__Exported_Functions()
============================================================================ */

/// Initialise SCI-A for serial display
void InitSciA(void);

/// Add values from FIFO to output buffer
void com_tx_fifo_run(void);

/// Reset FIFO
void com_tx_fifo_reset(void);

/// Low level add values to SCI-A buffer
void scia_xmit(uint16_t a);

/** @name Write variables to display */
//@{
/// add character to com_tx_fifo
void putc_com(uint16_t c);
/// add string to com_tx_fifo
void puts_com(char* str);

/// add 32bit single precision float to com_tx_fifo
void putf32_com(float32,uint16_t);

/// add signed 16bit value to display
void putd_com(int16_t bin);
/// add unsigned 16bit value to display
void putud_com(uint16_t bin);
/// add signed 32bit value to display
void putl_com(int32_t bin);
/// add unsignedf 32bit value to display
void putul_com(uint32_t bin);
/// add unsigned 64bit value to display
void putull_com(uint64_t bin);

/// add 16bit hex value to display
void putxx_com(uint16_t word);
/// add 32bit hex value to display
void putxxxx_com(uint32_t dword);

/// add 32bit float in scientific notation
void putsci_com(float32 real,uint16_t dig);
/// add 32bit float in engineering notation
void puteng_com(float32 real,uint16_t dig);
//@}

#endif //_SERIAL_COM_H_
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
