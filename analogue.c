/**
\file
\brief ADC functions and variables

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/

// compiler standard include files
#include <stdint.h>

// processor standard include files
#include "F2806x_Device.h"
#include "F2806x_GlobalPrototypes.h"

// local include files
#include "main.h"
#include "analogue.h"
#include "power_stage.h"
#include "fsm.h"
#include "cla.h"
#include "hardware.h"
#include "fault.h"

/*  =========================================================================
__Definitions()
============================================================================ */

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Variables()
============================================================================ */

/// Holds the analogue measured values
type_adc
	adc =
	{
		0,	// vin
		0,	// iac
		0,	// iboost
		0,	// vdc
		0,	// vzero
		0,VOUT_SCALE,VOUT_OFFSET,	// vout
		0,0,0,	// temp
		{
			0,	// vdc_average.sum
			0,	// vdc_average.real
			0,	// vdc_average.count
			0	// vdc_average.flag
		}
	};

/* =========================================================================
__Local_Function_Prototypes()
============================================================================ */

/// Interrupt on completion of ADC sequence read
#pragma CODE_SECTION(analogue_isr, "ramfuncs");
interrupt void analogue_isr(void);

/* =========================================================================
__Exported_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialises ADC

SOC0	A0	Garbage	ADCRESULT0

SOC2	A2	Iboost		ADCRESULT2
SOC3	B2	Vdc			ADCRESULT3

SOC4	A4	Iac			ADCRESULT4
SOC5	B4	Vin			ADCRESULT5
	EOC5	ADCINT1

SOC6	A6	AGND(ext)	ADCRESULT6
SOC7	B6	N/A			ADCRESULT7

SOC8	A5	Temperature	ADCRESULT8
SOC9	B5	AGND(int)	ADCRESULT9
	EOC9	ADCINT2

\author S.Parker
\par History:
\li	19/10/14 SP - initial creation
\li	16/05/15 SP - for Rev 2
*/
void InitADC(void)
{
	Uint16 i;

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.ADCENCLK = 1;

	/* Setup PIE for ADC interrupts */
	// ADCINT2 interrupt vector
	PieVectTable.ADCINT2 = &analogue_isr;

	// ADCINT2 in PIE
	PieCtrlRegs.PIEIER1.bit.INTx2 = 1;
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

	// CPU Interrupt mask for Group 1
	IER |= M_INT1;

	(*Device_cal)(); // Must be run before using ADC

	AdcRegs.ADCCTL1.bit.ADCBGPWD	= 1;	// Power up band gap
	AdcRegs.ADCCTL1.bit.ADCREFPWD	= 1;	// Power up reference
	AdcRegs.ADCCTL1.bit.ADCPWDN 	= 1;	// Power up rest of ADC
	AdcRegs.ADCCTL1.bit.ADCENABLE	= 1;

	for(i=0; i<7500; i++){}					// wait 90000 cycles = 1ms (each iteration is 12 cycles)

	AdcRegs.ADCCTL1.bit.INTPULSEPOS	= 0;	// Early interrupt pulse
	AdcRegs.ADCCTL1.bit.TEMPCONV = 1; // ADCINA5 is now internal temp sensor
	AdcRegs.ADCCTL1.bit.VREFLOCONV = 1; // ADCINB5 is now internal AGND

	AdcRegs.ADCCTL2.bit.CLKDIV2EN = 1; // Divide ADC clock by 2 as datasheet lists 45MHz max (90MHz/2 = 45MHz)

	AdcRegs.SOCPRICTL.bit.SOCPRIORITY = 0x10; // All SOCs are high priority mode

	// SOC0 on ePWM4 records Garbage (see Errata)
	AdcRegs.ADCSOC0CTL.bit.CHSEL 	= 0x0;    // set SOC0 channel select to ADCINA0
	AdcRegs.ADCSOC0CTL.bit.TRIGSEL 	= 0x0B;    // set SOC0 start trigger on EPWM4A
	AdcRegs.ADCSOC0CTL.bit.ACQPS 	= 6;	// set SOC0 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)
	
	// SOC2+3 on ePWM4 records Iboost and Vdc
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN2 = 1;
	AdcRegs.ADCSOC2CTL.bit.CHSEL 	= 0x2;    // set SOC2 channel select to ADCINA2/B2
	AdcRegs.ADCSOC2CTL.bit.TRIGSEL 	= 0x0B;    // set SOC2 start trigger on EPWM4A
	AdcRegs.ADCSOC2CTL.bit.ACQPS 	= 8;	// set SOC2 S/H Window to 9 ADC Clock Cycles, (8 ACQPS plus 1)

	// SOC4+5 on ePWM4 records Iac and Vin
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN4 = 1;
	AdcRegs.ADCSOC4CTL.bit.CHSEL 	= 0x4;    // set SOC4 channel select to ADCINA4/B4
	AdcRegs.ADCSOC4CTL.bit.TRIGSEL 	= 0x0B;    // set SOC4 start trigger on EPWM4A
	AdcRegs.ADCSOC4CTL.bit.ACQPS 	= 8;	// set SOC4 S/H Window to 9 ADC Clock Cycles, (8 ACQPS plus 1)

	// ADCINT1 at SOC5
	AdcRegs.INTSEL1N2.bit.INT1E     = 1;	// Enabled ADCINT1
	AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;	// Clear ADCINT1 flag
	AdcRegs.INTSEL1N2.bit.INT1CONT  = 1;	// Enable ADCINT1 Continuous mode
	AdcRegs.INTSEL1N2.bit.INT1SEL 	= 0x05;    // SOC3 to trigger ADCINT1

	// SOC6+7 on ePWM4 records AGND(ext) and Vout
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN6 = 1;
	AdcRegs.ADCSOC6CTL.bit.CHSEL 	= 0x6;    // set SOC6 channel select to ADCINA6/B6
	AdcRegs.ADCSOC6CTL.bit.TRIGSEL 	= 0x0B;    // set SOC6 start trigger on EPWM4A
	AdcRegs.ADCSOC6CTL.bit.ACQPS 	= 8;	// set SOC6 S/H Window to 9 ADC Clock Cycles, (8 ACQPS plus 1)

	// SOC8+9 on ePWM4 records Temperature and AGND(int)
	AdcRegs.ADCSAMPLEMODE.bit.SIMULEN8 = 1;
	AdcRegs.ADCSOC8CTL.bit.CHSEL 	= 0x5;    // set SOC8 channel select to ADCINA5
	AdcRegs.ADCSOC8CTL.bit.TRIGSEL 	= 0x0B;    // set SOC8 start trigger on EPWM4A
	AdcRegs.ADCSOC8CTL.bit.ACQPS 	= 25;	// set SOC8 S/H Window to 26 ADC Clock Cycles for accurate temperature measurement

	// ADCINT2 at SOC9
	AdcRegs.INTSEL1N2.bit.INT2E     = 1;	// Enabled ADCINT2
	AdcRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;	// Clear ADCINT2 flag
	AdcRegs.INTSEL1N2.bit.INT2CONT  = 0;	// Disable ADCINT2 Continuous mode (flag must be cleared)
	AdcRegs.INTSEL1N2.bit.INT2SEL 	= 0x09;    // SOC9 to trigger ADCINT2

	EDIS;

	/* Setup timing pin */
	EALLOW;
	GpioCtrlRegs.GPADIR.bit.GPIO27 = 1; // pin 61 as output
	GpioDataRegs.GPACLEAR.bit.GPIO27 = 1;
	EDIS;

	/* Temperature scalings */
	// This uses the F28069 built in scale and offset
	adc.temp.scale = (float32)getTempSlope()/65536.0;
	adc.temp.offset = (float32)getTempOffset();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialises DAC and comparator blocks for trip protection

\author S.Parker
\par History:
\li	31/10/14 SP - initial creation
\li	31/10/14 SP - for Rev 2
*/
void InitComparators(void)
{
	EALLOW;
	SysCtrlRegs.PCLKCR3.bit.COMP1ENCLK = 1;

	/* Comparator 1 for IBOOST trip */
	// Initialise comparator 1 using ADCA-A2
	Comp1Regs.COMPCTL.bit.SYNCSEL = 1; // Comparator output syncronised to clock
	Comp1Regs.COMPCTL.bit.QUALSEL = 0; // No qualificationp period
	Comp1Regs.COMPCTL.bit.CMPINV = 0; // Non-inverted output of comparator is passed
	Comp1Regs.COMPCTL.bit.COMPSOURCE = 0; // Inverting input connected to DAC

	// Initialise DAC 1 to required trip level
	Comp1Regs.COMPCTL.bit.COMPDACEN = 1; // Power up DAC logic
	Comp1Regs.DACCTL.bit.DACSOURCE = 0; // DAC controlled by DACVAL
	Comp1Regs.DACVAL.bit.DACVAL = IBOOST_OC_COMP;
	EDIS;

	EALLOW;
	SysCtrlRegs.PCLKCR3.bit.COMP2ENCLK = 1;
	SysCtrlRegs.PCLKCR3.bit.COMP3ENCLK = 1;

	/* Comparator 2 for IAC trip - positive */
	// Initialise comparator 2 using ADCA-A4
	Comp2Regs.COMPCTL.bit.SYNCSEL = 1; // Comparator output syncronised to clock
	Comp2Regs.COMPCTL.bit.QUALSEL = 0; // No qualification period
	Comp2Regs.COMPCTL.bit.CMPINV = 0; // Non-inverted output of comparator is passed
	Comp2Regs.COMPCTL.bit.COMPSOURCE = 0; // Inverting input connected to DAC

	// Initialise DAC 2 to required trip level
	Comp2Regs.COMPCTL.bit.COMPDACEN = 1; // Power up DAC logic
	Comp2Regs.DACCTL.bit.DACSOURCE = 0; // DAC controlled by DACVAL
	Comp2Regs.DACVAL.bit.DACVAL = IAC_OC_COMP_ZERO+IAC_OC_COMP;

	/* Comparator 3 for IAC trip - negative */
	// Initialise comparator 3 using ADCA-A6
	Comp3Regs.COMPCTL.bit.SYNCSEL = 1; // Comparator output syncronised to clock
	Comp3Regs.COMPCTL.bit.QUALSEL = 0; // No qualificationp period
	Comp3Regs.COMPCTL.bit.CMPINV = 1; // Inverted output of comparator is passed
	Comp3Regs.COMPCTL.bit.COMPSOURCE = 0; // Inverting input connected to DAC

	// Initialise DAC 2 to required trip level
	Comp3Regs.COMPCTL.bit.COMPDACEN = 1; // Power up DAC logic
	Comp3Regs.DACCTL.bit.DACSOURCE = 0; // DAC controlled by DACVAL
	Comp3Regs.DACVAL.bit.DACVAL = IAC_OC_COMP_ZERO-IAC_OC_COMP;
	EDIS;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns measured zero voltage

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
float32 get_adc_vzero(void)
{
	return adc.vzero.real;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns measured input voltage (scaled to volts)

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
float32 get_adc_vin(void)
{
	return adc.vin.real;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns measured output current (scaled to amps RMNS)

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
float32 get_adc_iac(void)
{
	return adc.iac.real;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns measured output voltage (scaled to volts RMS)

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
float32 get_adc_vout(void)
{
	return adc.vout.real;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns measured DC bus voltage (scaled to volts)

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
float32 get_adc_vdc(void)
{
	return adc.vdc.real;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns measured and averaged DC bus voltage (scaled to volts)

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
float32 get_adc_vdc_average(void)
{
	return adc.vdc_average.real;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns measured input boost current (scaled to amps)

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
float32 get_adc_iboost(void)
{
	return adc.iboost.real;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Returns MCU temperature (scaled to degrees C)

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
float32 get_adc_temp(void)
{
	return adc.temp.real;
}

/* =========================================================================
__Local_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Main CPU interrupt routine for measuring and scaling ADC values
Triggered by ADCINT2

\author S.Parker
\par History:
\li	04/10/14 SP - initial creation
*/
interrupt void analogue_isr(void)
{
	/* Set toggle bit */
	GpioDataRegs.GPASET.bit.GPIO27 = 1;

	/* Increment interrupt counter */
#ifndef PSIM_VERSION // begin PSIM_VERSION {1}	
	interrupt_counter.powerstage_isr++;
#endif // end PSIM_VERSION {1}

	/* Read and scale analogue inputs that arn't used by CLA */

	// Record 0V offset/reference with running average
	adc.vzero.real = (7.0*adc.vzero.real + (float32)AdcResult.ADCRESULT9)*0.125;
	cpu2cla.adc.vzero.real = adc.vzero.real;

	// Record output voltage
	adc.vout.real = ((float32)AdcResult.ADCRESULT7-adc.vzero.real)*adc.vout.scale-adc.vout.offset;  // counts * V/count  - V

	// Record temperature with running average
	adc.temp.real = (7.0*adc.temp.real + ((float32)AdcResult.ADCRESULT8-adc.temp.offset)*adc.temp.scale)*0.125;

	/* Record CLA processed analogues */
	adc.vdc.real = cla2cpu.adc.vdc.real;
	adc.iboost.real = cla2cpu.adc.iboost.real;
	adc.vin.real = cla2cpu.adc.vin.real;
	adc.iac.real = cla2cpu.adc.iac.real;

	/* Averaging */
	adc.vdc_average.sum += adc.vdc.real;
	adc.vdc_average.count++;
	if( (cla2cpu.phase.current < cla2cpu.phase.step)||
		(cla2cpu.phase.current+PHASE_180_DEG < cla2cpu.phase.step) )
	{
		adc.vdc_average.real = adc.vdc_average.sum/(float32)adc.vdc_average.count;
		adc.vdc_average.count = 0;
		adc.vdc_average.sum = 0;
	}
	cpu2cla.adc.vdc_average.real = adc.vdc_average.real;

	/* Fault detection */

	// Over current
	/*
	if(fabs(adc.iac.real) > IAC_OC_SOFT)
	{
		SWITCHING_DISABLE();
		FAULT_SET(FAULT_IAC_OC_SOFT);
	}
	if(adc.iboost.real > IBOOST_OC_SOFT)
	{
		SWITCHING_DISABLE();
		FAULT_SET(FAULT_IBOOST_OC_SOFT);
	}
	*/
	// Over voltage
	if(adc.vdc.real > VDC_OV_SOFT)
	{
		SWITCHING_DISABLE();
		FAULT_SET(FAULT_VDC_OV_SOFT);
	}

	/* Log grab data */
	// CPU or CLA grab data only (check cla.c)
/*	if(grab_state == GRAB_STATE_LOG)
	{
		if(grab_count < GRAB_LENGTH)
		{
			if(grab_decim_count == GRAB_DECIM)
			{
				grab_table[grab_count][0] = cla2cpu_sin_val*16384;
				grab_table[grab_count][1] = cla2cpu.iboost;
				grab_table[grab_count][2] = adc.vin;
				grab_table[grab_count][3] = adc.iac;
				grab_table[grab_count][4] = vdc_average.average;
				grab_table[grab_count][5] = adc.vout;
				grab_count++;
			}

			if(grab_decim_count == 0)
				grab_decim_count = GRAB_DECIM;
			else
				grab_decim_count--;
		}
		else
		{
			grab_log_done_flag = 1;
		}
	}
*/

	// Clear INT flag for ADCINT2
	AdcRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;
	// Acknowledge this interrupt to receive more interrupts from group 1
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

	/* Clear toggle bit */
	GpioDataRegs.GPACLEAR.bit.GPIO27 = 1;

}

// end analogue.c
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
