/**
\file
\brief Code entry point, background timer and serial interface

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
\li 29/12/14 SP - display and keyboard framework
\li 31/12/14 SP - grab code
*/

/**
\mainpage Antichinus Little Box Challenge - Introduction

\section _hw Hardware

This code was written for the TI TMS320F28069 Microcontroller as
installed on the Antichinus Controller Board Rev 2.

\section _cs Compiler Settings

The Texas Instrument TMS320C28x Optimizing C/C++ Compiler v15.12.2.LTS built
into Code Composer Studio V6 is used to compile, link and assemble the program.

\subsection _tilr TI Libraries Required

This firmware makes use of TI's peripheral libraries.
The required settings for --include_path:

\li ${CG_TOOL_ROOT}/include
\li ${LIBRARY_FOLDER}/Texas Instruments/f2803x
\li ${LIBRARY_FOLDER}/Texas Instruments/f2803x/v151/F2806x_common/include
\li ${LIBRARY_FOLDER}/Texas Instruments/f2803x/v151/F2806x_headers/include

The required TI library (V151) source files and libraries are:
\li DSP2803x_CodeStartBranch.asm
\li DSP2803x_GlobalVariableDefs.c
\li DSP2803x_Headers_nonBIOS.cmd

\subsection _ocf Other Compiler Flags

TBD

\subsection _ls Linker Settings

TBD
*/

// compiler standard include files
#include <stdint.h>

// processor standard include files
#include "F2806x_Device.h"
#include "F2806x_GlobalPrototypes.h"

// local include files
#include "fsm.h"
#include "main.h"
#include "analogue.h"
#include "power_stage.h"
#include "serial_com.h"
#include "cla.h"
#include "hardware.h"
#include "fault.h"

/*  =========================================================================
__Definitions()
============================================================================ */

/* =========================================================================
__Typedefs()
============================================================================ */

/* =========================================================================
__Variables()
============================================================================ */

/** @name Constants from specific memory locations */
//@{
/// Linker defined memory locations
extern uint16_t RamfuncsLoadStart, RamfuncsLoadEnd, RamfuncsRunStart;

/// Boot ROM sine table starts at 0x003FDF00 and has 641 entries of 32 bit sine
/// values making up one and a quarter periods (plus one entry). For 16 bit
/// values, use just the high word of the 32 bit entry. Peak value is 0x40000000.
int16_t
	*sin_table_fixed = (int16_t*)0x003FDF00,
	*cos_table_fixed = (int16_t*)0x003FE000;

/// Boot ROM sine table starts at 0x003FD860 and has 641 entries of single precision
/// values making up one and a quarter periods (plus one entry).
uint16_t *sin_table_float_ROM = (uint16_t*)0x003FD860;
#pragma DATA_SECTION(sin_table_float,"sinetable");
float32
	sin_table_float[641],
	*cos_table_float;
//@}

/// Used for monitoring interrupt cycles
type_int_counter interrupt_counter = {0,0};
	
/** @name Grab variables */
//@{
/// Grab state management
type_grab grab =
{
	GRAB_STATE_WAIT,
	0, // count
	0, // decim_count
	0, // log_flag
	0  // log_done_flag
};

/// Table of grab data - depending on defined grab type
#pragma DATA_SECTION(grab_table,"grabtable");
#ifdef GRAB_int16_t
	int16_t grab_table[GRAB_LENGTH][GRAB_WIDTH];
#endif
#ifdef GRAB_uint16_t
	uint16_t grab_table[GRAB_LENGTH][GRAB_WIDTH];
#endif
#ifdef GRAB_INT32
	int32_t grab_table[GRAB_LENGTH][GRAB_WIDTH];
#endif
#ifdef GRAB_FLOAT
	float32 grab_table[GRAB_LENGTH][GRAB_WIDTH];
	#pragma DATA_SECTION(cla2cpu_grab_data,"ClaToCpuMsgRAM");
	float32 cla2cpu_grab_data[GRAB_WIDTH];
#endif
//@}

/** @name System timer variables */
//@{
/// Idle time counter
uint64_t
	idle_count = 0,
	idle_record = 0;

/// Up time counter
uint32_t
	uptime_seconds = 0;

/// Superloop event timer
uint16_t
	event_timer = 0,
	new_event = 0;
//@}
	
/** @name Serial display variables */
//@{
uint16_t
	display_state = 0x200,
	skip = 0,
	analogue_index = 0;
//@}
	
/* =========================================================================
__Global_Function_Prototypes()
============================================================================ */

/// Initialise the operation of the flash memory
void InitFlash(void);

/// Basic initialisaiton of the microcontroller
void DeviceInit(void);

/// Enable interrupts in PIE and CPU
void EnableInterrupts(void);

/// Copy memory from one location to another
void MemCopy(uint16_t *SourceAddr, uint16_t* SourceEndAddr, uint16_t* DestAddr);

/* =========================================================================
__Local_Function_Prototypes()
============================================================================ */

/// Run the next display step
inline void serial_display(void);

/// Start new display line
inline void serial_display_newline(void);

/// Display help screen
inline void serial_display_help(void);

/// Read/poll last keyboard input
inline void serial_keyboard(void);

/// Increment dispalyed analogue variable
inline void serial_display_analogue_index_inc(void);

/// Decrement displayed analogue variable
inline void serial_display_analogue_index_dec(void);

/// Initialise grab subsystem
inline void InitGrab(void);

/* =========================================================================
__Local_Functions()
============================================================================ */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Code entry point and basic initialisation
Super loop and task timing

\author S.Parker
\par History:
\li	03/10/14 SP - initial creation
*/
void main(void)
{
	// Initialisation of clocks and GPIO
	DeviceInit();

	// Setup flash memory
	// Copy time critical code and Flash setup code to RAM
	// The  RamfuncsLoadStart, RamfuncsLoadEnd, and RamfuncsRunStart
	// symbols are created by the linker. Refer to the linker files.
	MemCopy(&RamfuncsLoadStart, &RamfuncsLoadEnd, &RamfuncsRunStart);
	// Call Flash Initialization to setup flash waitstates
	// This function must reside in RAM
	InitFlash();	// Call the flash wrapper init function
	
	InitGrab();

	// Copy sine table to RAM
	MemCopy(&sin_table_float_ROM[0], &sin_table_float_ROM[1282], (uint16_t*)&sin_table_float[0]);
	cos_table_float = &sin_table_float[128];

	// Setup CPU Timer 2 at 1ms
	CpuTimer2Regs.PRD.half.LSW = 90000&0x0000FFFF;	// 1ms interrupt @ 90MHz
	CpuTimer2Regs.PRD.half.MSW = (90000&0xFFFF0000)>>16;
	CpuTimer2Regs.TCR.bit.TIF = 1; // clear flag

	// Setup SCI-A for COM port
	InitSciA(); // at 57600baud, 8bit, 1stop, no parity

	puts_com("\r\n\nAntechinus LBC, Prototype: Rev2\r\n");
	puts_com("Compiled on "); puts_com(__DATE__); puts_com(" at "); puts_com(__TIME__); puts_com("\r\n\n");

	// Global interrupt enable
	EnableInterrupts();

	// Turn on fans
	FAN1_ON();
	FAN2_ON();

	// Setup CLA
	InitCLA();

	// Setup ADC
	InitADC();

	// Setup analogue comparators for protection
	InitComparators();

	// Setup boost and H-Bridge power stages
	InitBoost();
	InitHBridge();

	// Start ePWM TBCLK - ensures all PWM are exactly in sync
	START_EPWM_TBCLK();

	// Forever loop
	for(;;)
	{
		// Timed task handler
		if(CpuTimer2Regs.TCR.bit.TIF == 1)
		{
			CpuTimer2Regs.TCR.bit.TIF = 1;

			event_timer++;
			new_event++;

			/* 1ms events */
			// State machines
			run_system_fsm();
			run_boost_fsm();
			run_hbridge_fsm();

			// Write out to SCI FIFO
			com_tx_fifo_run();
		}
		else if(new_event > 0)
		{
			new_event--;
			// 10ms event slots
			switch(event_timer%10)
			{
				case 1:
					// Run the serial display output
					serial_display();
					break;
				case 2:
					// Grab state control
					switch(grab.state)
					{
						case GRAB_STATE_WAIT:
							if(grab.log_flag == 1) // flag to start logging
							{
								grab.decim_count = GRAB_DECIM;
								grab.count = 0;
								grab.log_flag = 0;
								grab.state = GRAB_STATE_LOG;
							}
							break;
						case GRAB_STATE_LOG:
							if(grab.log_done_flag == 1) // flag that logging is finished
							{
								grab.state = GRAB_STATE_WAIT;
								grab.log_done_flag = 0;
							}
							break;
					}
					break;
				case 3:
					break;
				case 4:
					break;
				case 5:
					break;
				case 6:
					break;
				case 7:
					break;
				case 8:
					break;
				case 9:
					break;
			}
			// 100ms event slots
			switch(event_timer%100)
			{
				case 10:
					// Check for serial input commands
					serial_keyboard();
					break;
				case 20:
					break;
				case 30:
					break;
				case 40:
					break;
				case 50:
					break;
				case 60:
					break;
				case 70:
					break;
				case 80:
					break;
				case 90:
					break;
			}
			// 1s event slots
			switch(event_timer%1000)
			{
				case 100:
					LED2_SET();
					uptime_seconds++;
					break;
				case 200:
					// Trigger new display line
					serial_display_newline();
					break;
				case 300:
					break;
				case 400:
					break;
				case 500:
					break;
				case 600:
					LED2_CLEAR();
					break;
				case 700:
					// Trigger new display line
					serial_display_newline();
					break;
				case 800:
					break;
				case 900:
					break;
				case 0: //1000
					// Start timer again
					event_timer = 0;
					break;
			}
		}
	}

	/* BEWARE: Dragons be here. */

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Output display lines to serial port
Runs the print half of the grab code state machine

\author S.Parker
\par History:
\li	29/12/14 SP - initial creation
*/
inline void serial_display(void)
{
	uint16_t i;

	/* Regular display states */
	switch(display_state)
	{
		case 0:
			PUTNEWLINE_COM();
			putul_com(uptime_seconds);
			putc_com(' ');
			break;
		case 1:
			puts_com("S:");
			puts_com(get_system_state_name());
			puts_com(" || ");
			break;
		case 2:
			puts_com("B:");
			puts_com(get_boost_state_name());
			putc_com(' ');
			break;
		case 3:
			if(is_boost_openloop())
			{
				putf32_com(get_boost_dutycycle()*0.01,2);
				puts_com("dc");
			}
			else // boost is stopped
			{

			}
			puts_com(" || ");
			break;
		case 4:
			puts_com("H:");
			puts_com(get_hbridge_state_name());
			putc_com(' ');
			break;
		case 5:
			if(is_hbridge_open_loop())
			{
				putf32_com(get_hbridge_moddepth()*0.01,2);
				puts_com("md");
			}
			else if(is_hbridge_dc_bus_comp())
			{
				putf32_com(get_hbridge_voltage(),0);
				puts_com("Vrms");
			}
			else // hbridge is stopped
			{

			}
			puts_com(" || ");
			break;
		case 6:
			putxx_com(fault_get());
			putc_com(' ');
			break;
		case 7:
			putf32_com(get_adc_temp(),1);
			putc_com(0xB0);
			puts_com("C ");
			putl_com(getTempSlope());
			puts_com(" ");
			putl_com(getTempOffset());
			puts_com(" ");
			break;
		case 8:
			puts_com("idx:");
			putud_com(analogue_index);
			putc_com(' ');
			display_state = ANALOGUE_DISPLAY_START + analogue_index - 1;
			break;

		/* Analogue Index */
		case ANALOGUE_DISPLAY_START:
			putf32_com(get_adc_vin(),1);
			puts_com("Vin");
			display_state = IDLE_DISPLAY_STATE;
			break;
		case ANALOGUE_DISPLAY_START+1:
			putf32_com(get_adc_vdc(),1);
			puts_com("Vdc");
			display_state = IDLE_DISPLAY_STATE;
			break;
		case ANALOGUE_DISPLAY_START+2:
			putf32_com(get_adc_vdc_average(),1);
			puts_com("Vdc_av");
			display_state = IDLE_DISPLAY_STATE;
			break;
		case ANALOGUE_DISPLAY_START+3:
			putf32_com(get_adc_vout(),1);
			puts_com("Vac");
			display_state = IDLE_DISPLAY_STATE;
			break;
		case ANALOGUE_DISPLAY_START+4:
			putf32_com(get_adc_iboost(),2);
			puts_com("Iboost");
			display_state = IDLE_DISPLAY_STATE;
			break;
		case ANALOGUE_DISPLAY_START+5:
			putf32_com(get_adc_iac(),2);
			puts_com("Iac");
			display_state = IDLE_DISPLAY_STATE;
			break;
		case ANALOGUE_DISPLAY_START+6:
			putf32_com(get_adc_vzero(),2);
			puts_com("Vzero_cnt");
			display_state = IDLE_DISPLAY_STATE;
			break;
		case ANALOGUE_DISPLAY_START+7:
			putf32_com(0.0,1);
			puts_com("empty");
			display_state = IDLE_DISPLAY_STATE;
			break;

		/* Help display states */
		case HELP_DISPLAY_START:
			PUTNEWLINE_COM();
			break;
		case HELP_DISPLAY_START+1:
			puts_com("See main.c function: inline void serial_keyboard(void)");
			PUTNEWLINE_COM();
			break;

		/* Grab display states */
		case GRAB_DISPLAY_START:
			grab.count = 0;
			skip = 0;

			// Print out nice list of index pronumerals
			puts_com("\r\ni");
			for(i=0; i<GRAB_WIDTH; i++)
			{
				putc_com('\t');
				putc_com(i + 'A');
			}
			break;
		case GRAB_DISPLAY_START+1:
			if(skip == 4) // Skip used to slow down display rate to prevent tx fifo trunction
			{
				PUTNEWLINE_COM();
				putud_com(grab.count);

				for(i=0; i<GRAB_WIDTH; i++)
				{
					putc_com('\t');

					#ifdef GRAB_int16_t
					putd_com(grab_table[grab.count][i]);
					#endif

					#ifdef GRAB_uint16_t
					putud_com(grab_table[grab.count][i]);
					#endif

					#ifdef GRAB_INT32
					putl_com(grab_table[grab.count][i]);
					#endif

					#ifdef GRAB_FLOAT
					putf32_com(grab_table[grab.count][i],5);
					#endif
				}

				grab.count++;

				if(grab.count != GRAB_LENGTH)
				{
					display_state = GRAB_DISPLAY_START;
				}

				skip = 0;
			}
			else
			{
				skip++;

				if(grab.count != GRAB_LENGTH)
				{
					display_state = GRAB_DISPLAY_START;
				}
			}
			break;
		case GRAB_DISPLAY_START+2:
			grab.state = GRAB_STATE_WAIT;
			PUTNEWLINE_COM();
			break;

		/* Idle display states */
		case IDLE_DISPLAY_STATE:
		default:
			display_state = IDLE_DISPLAY_STATE;
			break;
	}

	display_state++;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Start a new line of the serial display

\author S.Parker
\par History:
\li	29/12/14 SP - initial creation
*/
inline void serial_display_newline(void)
{
	if(display_state >= IDLE_DISPLAY_STATE)
	{
		display_state = 0;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Display help screen

\author S.Parker
\par History:
\li	29/12/14 SP - initial creation
*/
inline void serial_display_help(void)
{
	display_state = HELP_DISPLAY_START;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Display grab data

\author S.Parker
\par History:
\li	29/12/14 SP - initial creation
*/
inline void serial_display_grab(uint16_t en)
{
	if((en)&&(grab.state==GRAB_STATE_WAIT))
	{
		display_state = GRAB_DISPLAY_START;
	}
	else
	{
		display_state = 0;
	}
}

inline void serial_display_analogue_index_inc(void)
{
	if(analogue_index == 7)
	{
		analogue_index = 0;
	}
	else
	{
		analogue_index++;
	}
}
inline void serial_display_analogue_index_dec(void)
{
	if(analogue_index == 0)
	{
		analogue_index = 7;
	}
	else
	{
		analogue_index--;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Keyboard input from serial port

Keyboard character -> command
GLOBAL
	?	->	Print help screen

SYSTEM
- S:Hold
	Q -> Enable development mode
	W -> Enable production mode
- S:Dev
	t/T -> Toggle Boost mode
	y/Y -> Toggle HBridge mode

	e/E -> Enable Boost switching
	r/R -> Enable HBridge switching
	d/D -> Disable ALL switching

	F	-> 	Clear all faults

	g	->	Get grab data
	h	->	Print grab data
	H	->	Stop printing grab data
	a/A	->	Analogue index inc/dec
	Q/W -> Disable development mode
- S:Prod
	Q/W -> Disable production mode

BOOST
- B:OLx (open loop)
	i/I	->	Increase boost duty cycle +1% / +10%
	m/M	->	Decrease boost duty cycle -1% / -10%
- current regulation
- DC bus regulation

HBRIDGE
- H:OLx (open loop)
	o/O	->	Increase hbridge modulation depth +1% / +10%
	,/<	->	Decrease hbridge modulation depth -1% / -10%
- H:DCx (DC bus compensation)
	o/O	->	Increase hbridge RMS voltage target +20V / +2V
	,/<	->	Decrease hbridge RMS voltage target -20V / -2V

History:	29/12/14	SGP - Creation
*/
inline void serial_keyboard(void)
{
	uint16_t key_input;

	if(SciaRegs.SCIFFRX.bit.RXFFST)
	{
		// Record key press
		key_input = SciaRegs.SCIRXBUF.bit.RXDT;
	}
	else
	{
		// No key press
		return;
	}

	// Global commands
	switch(key_input)
	{
		case '?':
			serial_display_help();
			return;
		// increment/decrement analogue index
		case 'a':
			serial_display_analogue_index_inc();
			return;
		case 'A':
			serial_display_analogue_index_dec();
			return;
	}

	// Key commands on startup
	if(background.system_mode==SYS_MODE_NONE)
	{
		switch(key_input)
		{
			case 'Q':
				background.system_mode = SYS_MODE_DEV;
				return;
			case 'W':
				//background.system_mode = SYS_MODE_PROD;
				return;
		}
	}

	// Key commands in development mode
	if(background.system_mode==SYS_MODE_DEV)
	{
		switch(key_input)
		{
			// Toggle hbridge mode
			case 'y':
			case 'Y':
				if(is_hbridge_hold())
				{
					switch(get_hbridge_mode())
					{
						case HBRIDGE_MODE_NONE:
							set_hbridge_mode(HBRIDGE_MODE_OPEN_LOOP);
							break;
						case HBRIDGE_MODE_OPEN_LOOP:
							set_hbridge_mode(HBRIDGE_MODE_DC_BUS_COMP);
							break;
						case HBRIDGE_MODE_DC_BUS_COMP:
							set_hbridge_mode(HBRIDGE_MODE_NONE);
							break;
						default:
							set_hbridge_mode(HBRIDGE_MODE_NONE);
							break;
					}
				}
				return;

			// Toggle boost mode
			case 't':
			case 'T':
				if(is_boost_hold())
				{
					switch(get_boost_mode())
					{
						case BOOST_MODE_NONE:
							set_boost_mode(BOOST_MODE_OPEN_LOOP);
							break;
						case BOOST_MODE_OPEN_LOOP:
							set_boost_mode(BOOST_MODE_CURRENT_REGULATION);
							break;
						case BOOST_MODE_CURRENT_REGULATION:
							set_boost_mode(BOOST_MODE_BUS_REGULATION);
							break;
						case BOOST_MODE_BUS_REGULATION:
							set_boost_mode(BOOST_MODE_NONE);
							break;
						default:
							set_boost_mode(BOOST_MODE_NONE);
							break;
					}
				}
				return;

			// Enable hbridge switching
			case 'r':
			case 'R':
				enable_hbridge();
				return;
			// Enable boost switching
			case 'e':
			case 'E':
				enable_boost();
				return;
			// Disable all
			case 'd':
			case 'D':
				disable_hbridge();
				disable_boost();
				return;

			// Clear faults
			case 'F':
				fault_clear_all();
				return;

			// Initiate data grab
			case 'g':
				grab.log_flag = 1;
				return;

			// Display grab
			case 'h':
				serial_display_grab(1);
				return;

			// Stop grab display
			case 'H':
				serial_display_grab(0);
				return;

			// Boost Duty Cycle - Increase
			case 'i':
				if(is_boost_openloop())
					set_boost_dutycycle(get_boost_dutycycle() + 1);
				return;
			case 'I':
				if(is_boost_openloop())
					set_boost_dutycycle(get_boost_dutycycle() + 10);
				return;

			// Boost Duty Cycle - Decrease
			case 'm':
				if(is_boost_openloop())
					set_boost_dutycycle(get_boost_dutycycle() - 1);
				return;
			case 'M':
				if(is_boost_openloop())
					set_boost_dutycycle(get_boost_dutycycle() - 10);
				return;

			// HBridge Mod Depth/RMS Voltage - Increase
			case 'o':
				if(is_hbridge_open_loop())
					set_hbridge_moddepth(get_hbridge_moddepth() + 1);
				else if(is_hbridge_dc_bus_comp())
					set_hbridge_voltage(get_hbridge_voltage() + 2);
				return;
			case 'O':
				if(is_hbridge_open_loop())
					set_hbridge_moddepth(get_hbridge_moddepth() + 10);
				else if(is_hbridge_dc_bus_comp())
					set_hbridge_voltage(get_hbridge_voltage() + 20);
				return;

			// HBridge Mod Depth/RMS Voltage - Decrease
			case ',':
				if(is_hbridge_open_loop())
					set_hbridge_moddepth(get_hbridge_moddepth() - 1);
				else if(is_hbridge_dc_bus_comp())
					set_hbridge_voltage(get_hbridge_voltage() - 2);
				return;
			case '<':
				if(is_hbridge_open_loop())
					set_hbridge_moddepth(get_hbridge_moddepth() - 10);
				else if(is_hbridge_dc_bus_comp())
					set_hbridge_voltage(get_hbridge_voltage() - 20);
				return;

			// Exit development mode
			case 'Q':
			case 'W':
				if(is_boost_hold()&&is_hbridge_hold())
					background.system_mode = SYS_MODE_NONE;
				return;
		}
	}

	// Key commands in production modes
	if(background.system_mode==SYS_MODE_PROD)
	{
		switch(key_input)
		{
			// Exit production mode
			case 'Q':
			case 'W':
				background.system_mode = SYS_MODE_NONE;
				return;
		}
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/**
Initialises grab variables

History:	31/12/14	SGP - Creation
*/
inline void InitGrab(void)
{
	uint16_t i, j;

	// Zero the grab table
	for(i=0; i<GRAB_WIDTH; i++)
	{
		for(j=0; j<GRAB_LENGTH; j++)
		{
			grab_table[j][i] = 0;
		}
	}

	grab.state = GRAB_STATE_WAIT;
	grab.decim_count = GRAB_DECIM;
	grab.count = 0;
	grab.log_flag = 0;
	grab.log_done_flag = 0;
}

// end main.c
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
